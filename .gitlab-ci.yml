# CI/CD workflow for bayeswave
# 1. Build a container with conda env dependences.  Scheduled builds produce
# the "latest" image; tags produce a tagged version of the dependencies at the
# time of that tag.
# 2. Build the bayeswave package from source in the conda env image and docs pages
# 3. Test major executables
# 4. Build & push the bayeswave runtime container from BayesWave source on top of the
# conda env dependencies.
# 5. Push documentation for tagged commits only
#
# TODO: build the env image only for master
# TODO: use the upstream for the env image

variables:
  BRANCH: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  COMMIT: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  UPSTREAM_REGISTRY: containers.ligo.org/lscsoft/bayeswave
  CONDA_ENV_IMAGE: conda-env
  BUILD_DIR: test-install
  BUILD_TARGET: $CI_PROJECT_DIR/$BUILD_DIR
  TEST_OUTPUT: test-output

stages:
  - docker
  - build
  - test
  - deploy

.docker_template: &docker_deploy
  image: docker
  before_script:
    - echo "Logging in"
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
  script:
    - if [ -z $CI_COMMIT_TAG ]; then IMAGE_TAG="latest"; else IMAGE_TAG="$CI_COMMIT_TAG"; fi
    - if [ -z $IMAGE_NAME ]; then IMAGE="$CI_REGISTRY_IMAGE:$IMAGE_TAG"; else IMAGE="$CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG" ; fi
    - echo "Building image - $IMAGE"
    - docker build --no-cache
        --build-arg CI_COMMIT_SHA=${CI_COMMIT_SHA}
        --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ')
        --build-arg BUILD_IMAGE=$UPSTREAM_REGISTRY/$CONDA_ENV_IMAGE:$IMAGE_TAG
        -t $IMAGE --file $DOCKERFILE .
    - docker push $IMAGE

# Build the conda environment dependencies
conda-env:
  stage: docker
  <<: *docker_deploy
  variables:
    IMAGE_NAME: $CONDA_ENV_IMAGE
    DOCKERFILE: .conda-env.Dockerfile
  only:
    refs:
      - tags
      - master@lscsoft/bayeswave

# FIXME:  reconcile with above - SHORT TERM HACK (seriously - this could break paperwork)
# Build the conda environment dependencies
conda-env-manual:
  stage: docker
  <<: *docker_deploy
  variables:
    IMAGE_NAME: $CONDA_ENV_IMAGE
    DOCKERFILE: .conda-env.Dockerfile
  when: manual

# -------------------------------------------------------
# BUILD

# Build bayeswave from source
bayeswave:
  stage: build
  image: $UPSTREAM_REGISTRY/$CONDA_ENV_IMAGE:latest
  script:
    - mkdir -p build
    - cmake . -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=true -DCMAKE_INSTALL_PREFIX=$BUILD_DIR
    - cmake --build . -- VERBOSE=1
    - cmake --build . --target install
  artifacts:
    expire_in: 1h
    paths:
      - $BUILD_DIR

BayesWaveUtils:
  stage: build
  image: $UPSTREAM_REGISTRY/$CONDA_ENV_IMAGE:latest
  script: 
    - pushd BayesWaveUtils
    - python setup.py install --prefix $BUILD_TARGET
    - popd
  artifacts:
    expire_in: 1h
    paths:
      - $BUILD_DIR

# Build environment script
env-script:
  stage: build
  script:
    - mkdir -p $BUILD_TARGET
    - sed "s|INSTALL_DIR|$BUILD_DIR|g" $CI_PROJECT_DIR/etc/bayeswave-user-env.sh > $BUILD_TARGET/bayeswave-user-env.sh
  artifacts:
    expire_in: 1h
    paths:
      - $BUILD_DIR
docs:
  stage: build
  image: python:3.7-slim-stretch
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  before_script:
    # install pandoc >= 2.0.0
    - apt-get -yqq update
    - apt-get -yqq install curl
    - curl --location --output pandoc.deb https://github.com/jgm/pandoc/releases/download/2.7.2/pandoc-2.7.2-1-amd64.deb
    - dpkg --install pandoc.deb ||  apt-get -y -f install; dpkg --install pandoc.deb; 
    # install python dependencies
    - python3 -m pip install -r doc/requirements.txt
  script:
    - bash -ex doc/build.sh
  artifacts:
    expire_in: 6h
    paths:
      - doc/_build
  cache:
    paths:
      - .cache/pip
  only:
      - tags

# -------------------------------------------------------
# TEST

# Test bayeswave functionality
BayesWave:
  stage: test
  image: $UPSTREAM_REGISTRY/$CONDA_ENV_IMAGE:latest
  script:
    - source $BUILD_DIR/bayeswave-user-env.sh
    - cat $BUILD_DIR/bayeswave-user-env.sh
    - ls -R $BUILD_DIR 
    - BayesWave --help
    - "BayesWave --ifo H1 --H1-flow 32 \
      --H1-cache LALSimAdLIGO --H1-channel LALSimAdLIGO \
      --trigtime 900000000.00 --srate 512 --seglen 4 --PSDstart 900000000 \
      --PSDlength 1024 --NCmin 2 --NCmax 2 --dataseed 1234 \
      --Niter 500 --outputDir $TEST_OUTPUT"
  dependencies:
    - env-script
    - bayeswave

# Test bayeswavePost functionality
BayesWavePost:
  stage: test
  image: $UPSTREAM_REGISTRY/$CONDA_ENV_IMAGE:latest
  script:
    - source $BUILD_DIR/bayeswave-user-env.sh
    - cat $BUILD_DIR/bayeswave-user-env.sh
    - ls -R $BUILD_DIR 
    - BayesWavePost --help
  dependencies:
    - env-script
    - bayeswave

bayeswave_pipe:
  stage: test
  image: $UPSTREAM_REGISTRY/$CONDA_ENV_IMAGE:latest
  script:
    - ls $BUILD_DIR
    - source $BUILD_DIR/bayeswave-user-env.sh
    - bayeswave_pipe --help
  dependencies:
    - env-script
    - BayesWaveUtils

# FIXME: c'mon now.  Someone should add an argparser to these codes so there is SOME kind of usage info

      # megaplot:
      #   stage: test
      #   image: $CONDA_ENV_IMAGE
      #   script:
      #     - ls $BUILD_DIR
      #     - source $BUILD_DIR/bayeswave-user-env.sh
      #     - megaplot.py --help
      #   dependencies:
      #     - env-script
      #     - BayesWaveUtils
      #
      # megasky:
      #   stage: test
      #   image: $CONDA_ENV_IMAGE
      #   script:
      #     - ls $BUILD_DIR
      #     - source $BUILD_DIR/bayeswave-user-env.sh
      #     - megasky.py --help
      #   dependencies:
      #     - env-script
      #     - BayesWaveUtils

# -------------------------------------------------------
# DEPLOY

# Latest image - install from master
bayeswave-runtime:latest:
  stage: deploy
  image: docker:latest
  <<: *docker_deploy
  variables:
    DOCKERFILE: Dockerfile
  only:
      - master@lscsoft/bayeswave

bayeswave-runtime:tag:
  stage: deploy
  image: docker:latest
  <<: *docker_deploy
  variables:
    DOCKERFILE: Dockerfile
  only:
      - tags

pages:
  stage: deploy
  dependencies:
    - docs
  only:
    - tags
      #- tags@lscsoft/bayeswave
  script:
    - mv doc/_build/html public
  artifacts:
    expire_in: 6h
    paths:
      - public
