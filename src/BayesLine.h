/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <getopt.h>
#include <time.h>
#include <assert.h>

#include <gsl/gsl_sort.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_linalg.h>
#include <omp.h>

#define TPI 6.2831853071795862319959269370884
#define lAwidth 2.3    // 2.3 is one decade
#define ZETA 1.0
#define kappa_BL 0.8
#define ufrac 0.4    // rj unifrom draw fraction 
#define FSTEP 10.0   // the stencil separation in Hz for the spline model. Going below 2 Hz is dangerous - will fight with line model
#define HMean 1      //0:Average 1:Harmonic Mean
#define linemul 9.0  // how much above the Gaussian floor a line needs to be
#define dfmin 2.0    // minimum smooth spacing
#define lowlat 1     //run on low latency settings to get  quick start psd model

//static const gsl_rng_type *rngtype;
//static const gsl_rng *rng;

//##############################################
//OPEN MP

//gsl_rng **rvec;
//##############################################

typedef struct
{
  int n;
  int size;

  int *larray;

  double *Q;
  double *A;
  double *f;

}lorentzianParams;

typedef struct
{
  int tmax;
  int ncut;
  int nmin;
  int sgmts;

  double df;
  double fny;
  double Tobs;
  double fmin;
  double fmax;
  double flow;
  double fgrid;
  double fstep;
  double fhigh;
  double cadence;

}dataParams;

typedef struct
{
  int n;
  double *points;
  double *data;

}splineParams;

typedef struct
{
  double SAmin;
  double SAmax;
  double LQmin;
  double LQmax;
  double LAmin;
  double LAmax;

  //double *invsigma; //variances for each frequency bin
  double *sigma; //variances for each frequency bin
  double *upper; //variances for each frequency bin
  double *lower; //variances for each frequency bin
  double *mean;     //means for each frequency bin
  // double *upperSb; //variances for each frequency bin

}BayesLinePriors;

struct BayesLineParams
{
  dataParams *data;
  splineParams *spline;
  splineParams *spline_x;
  lorentzianParams *lines_x;
  BayesLinePriors *priors;

  double *Snf;
  double *Sna;
  double *fa;
  double *freq;
  double *power;
  double *spow;
  double *sfreq;
  double *Sbase;
  double *Sline;
  int maxBLLines;
  int constantLogLFlag;
  int flatPriorFlag;
  
  double TwoDeltaT;
  gsl_rng *r;
};

void BayesLineFree(struct BayesLineParams *bptr);
void BayesLineSetup(struct BayesLineParams *bptr, double *freqData, double fmin, double fmax, double deltaT, double Tobs);

void BayesLineRJMCMC(struct BayesLineParams *bayesline, double *freqData, double *psd, double *invpsd, double *splinePSD, int N, int cycle, double beta, int priorFlag, double *fprior, int SplineFlag, int SnModel);
void BayesLineInitialize(struct BayesLineParams *bayesline);

void BayesLineLorentzSplineMCMC        (struct BayesLineParams *bayesline, double heat, int steps, int focus, int priorFlag, double *dan, double *fprior, int SplineFlag, int SnModel);

double loglike_fit_spline(double *respow, double *Snf, int ncut);

double loglike_pm        (double *respow, double *Sn, double *Snx, int ilow, int ihigh);
double loglike_single    (double *respow, double *Sn, double *Snx, int ilowx, int ihighx, int ilowy, int ihighy);

double qdraw(double *fprop, double pmax, double flow, double fhigh, int ncut, double Tobs, gsl_rng *r);
double lprop(double f, double *fprop, dataParams *data);

void full_spectrum_single(double *Sn, double *Snx, double *Sbasex, double *sfreq, dataParams *data, lorentzianParams *line_x, lorentzianParams *line_y, int ii, int SnModel, int *ilowx, int *ihighx, int *ilowy, int *ihighy);
void full_spectrum_add_or_subtract(double *Snew, double *Sold, double *Sbase, double *sfreq, dataParams *restrict data, lorentzianParams *restrict lines, int ii, int SnModel, int *ilow, int *ihigh, int flag, int lineflag);
void full_spectrum_spline(double *Sline, double *Sbase, double *sfreq, dataParams *restrict data, lorentzianParams *restrict lines, int SnModel);

void spectrum_spline(double *Sn, double *Sbase, double *sfreq, dataParams *data, lorentzianParams *restrict lines, splineParams *restrict spline, int SplineFlag, int SnModel);

void spectrum_lowlat(double *data, double *S, double *Sn, double *Smooth, double df, int N, double *fprior);

void AkimaSplineGSL_one(int N, double *x, double *y, double xint, double *yint);
void CubicSplineGSL_one(int N, double *x, double *y, double xint, double *yint);
void CubicSplineGSL(int imin, int imax, int N, double *x, double *y, int Nint, double *xint, double *yint);
void AkimaSplineGSL(int imin, int imax, int N, double *x, double *y, int Nint, double *xint, double *yint);
void getrangeakima(int iu, int nsy, double *x, dataParams *restrict data, int Nend, int *imin, int *imax);
double delta_loglike(double *respow, double *Sn, double *Snx, int imin, int imax);

double rjdraw(double model, double sp, double prange, double pmin, gsl_rng *r);
double rjden(double model, double ref, double sp, double prange);
    
void create_dataParams(dataParams *data, double *f, int n,int max_lines);

void create_lorentzianParams(lorentzianParams *lines, int size);
void copy_lorentzianParams(lorentzianParams *origin, lorentzianParams *copy);
void destroy_lorentzianParams(lorentzianParams *lines);

void create_splineParams(splineParams *spline, int size);
void copy_splineParams(splineParams *origin, splineParams *copy);
void destroy_splineParams(splineParams *spline);

void copy_bayesline_params(struct BayesLineParams *origin, struct BayesLineParams *copy);
void print_line_model(FILE *fptr, struct BayesLineParams *bayesline);
void print_spline_model(FILE *fptr, struct BayesLineParams *bayesline);
void parse_line_model(FILE *fptr, struct BayesLineParams *bayesline);
void parse_spline_model(FILE *fptr, struct BayesLineParams *bayesline);

void isums(double x, double *is);
void solve(int n, double **M, double *av, double *yv);
void splinespace(int Ns, int istart, int iend, double *SM, double *freqs, double Tobs, double *splineA, double *splinef, int *Nk);
void getrange(int k, int Nknot, double *ffit, double Tobs, int *imin, int *imax);
double line(double f, double linef, double lineh, double linew, double deltafmax, double lineQ);
void lineset(int Ns, int istart, int iend, double *SM, double *PS, double *freqs, double Tobs, double *lf, double *lh, double *lQ, double *lw, double *dfmx, int max_lines, int *Nlns, int SnModel);
void makespec(double *SN, double *SM, double *SL, double *freqs, int istart, int iend, int Nknot, int Nlines, double *splinef, double *splineA, double *linef, double *lineh, double *linew, double *deltafmax, double *lineQ, int SnModel);
void MaxLine(double *PS, double *SNA, double *SMA, double *lf, double *lh, double *lQ, double *lw, double *splinef, double *splineA, int *Nknt, int *Nln, double Tobs, double fmin, double fmax, int SnModel);
void psdstart_update(int mc, int m, int q, int istart, int iend, double *logLx, double heat, double smsc, double lnsc, double Tobs, int Ns, int Nknot, int Nlines, double *freqs, double *PS, double *LarrayX, double *SM, double *SL, double *SN, double *splinefx, double *splineAx, double  *linef, double *lineh, double *lineQ, double *linew, double *deltafmax, int *cS, int *cL, int *acS, int *acL, int SnModel, gsl_rng *r);
void trim(double **splinefx, double **splineAx, int *Nknot, double *freqs, double **SN, double **SM, double **SL, double *PS, double **LarrayX, double *logLx, int istart, int iend, double Tobs, int NC, int SnModel);
    
void blstartsub(int N, int *Nsp, int *Nl, double *dspline, double *pspline, double *linef, double *lineh, double *lineQ, double *linew, int max_lines, char *ifo, double *Sn, double *specD, double *sspecD, double df, double Tobs, double fmin, double fmax, double fac, int SplineFlag, int SnModel);
void BayesLineBurnin(struct BayesLineParams *bayesline, double *timeData, double *freqData, char *ifo, double *fprior, int SplineFlag, int SnModel, int Nthread);


