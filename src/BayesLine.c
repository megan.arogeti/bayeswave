/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

/*********************************************************************************/
/*                                                                               */
/*     BayesLine fits the LIGO/Virgo power spectra using a model made up         */
/*     of N Lorentzian lines (described by central frequency f, quality          */
/*     factor Q and amplitude A) and cubic spline with M control points.         */
/*     The number of terms in each model, N, M, are free to vary via RJMCMC      */
/*     updates. The code initializes the models in a non-Markovian fashion,      */
/*     then refines the models with a full Markovian RJMCMC subroutine. This     */
/*     subroutine (LorentzMCMC) can be called by other codes to update the       */
/*     spectral fit to the residuals (data - signal model). Doing this is        */
/*     highly recommended, as it ensures that the spectral model is not          */
/*     eating any of the gravitational wave signal. Since the code is            */
/*     transdimensional (and very free in its movement between dimensions)       */
/*     it will not "over-fit" the spectral model.                                */
/*                                                                               */
/*********************************************************************************/


#include "BayesLine.h"
#include <omp.h>

#ifndef _OPENMP
#define omp ignore
#endif

//##############################################
//OPEN MP
gsl_rng **rvec;
//##############################################
/*
static void system_pause()
{
  printf("Press Any Key to Continue\n");
  getchar();
}
*/

double qdraw(double *fprop, double pmax, double flow, double fhigh, int ncut, double Tobs, gsl_rng *r)
{
    int i;
    double alpha, beta;
    double f;
    
    do
    {
     f = flow + (fhigh-flow)*gsl_rng_uniform(r);  // draw a frequency
     i = (int)(floor((f-flow)*Tobs)); // map to a bin
     if(i <0) i = 0;
     if(i > ncut-1) i = ncut-1;
     beta = fprop[i]/pmax;
     alpha = gsl_rng_uniform(r);
    }while(alpha > beta);
    
    return f;
    
}

double lprop(double f, double *fprop, dataParams *data)
{
  int i;

  int ssize    = data->ncut;
  double flow  = data->flow;
  double Tobs  = data->Tobs;

  i = (int)((f-flow)*Tobs);
  if(i < 0) i=0;
  else if(i > ssize-1) i=ssize-1;

  return(log(fprop[i]));
}

double loglike_fit_spline(double *respow, double *Snf, int ncut)
{
  double lgl, x;
  int i;

  lgl = 0.0;
  for(i=0; i< ncut; i++)
  {
    x = (respow[i]-Snf[i])*(respow[i]-Snf[i])/0.1;
    lgl -= (x);
  }

  return(lgl);
}

//static double logprior(double *invsigma, double *mean, double *Snf, int ilow, int ihigh)
//static double logprior(double *sigma, double *mean, double *Snf, int ilow, int ihigh)
static double logprior(double *lower, double *upper, double *Snf, int ilow, int ihigh)
{
  //double x;
  double lgp,dS;
  int i;

  //leaving off normalizations since they cancel in Hastings ratio
  lgp = 0;
  for(i=ilow; i<ihigh; i++)
  {
    if(Snf[i]>upper[i])
    {
      dS = log(Snf[i]) - log(upper[i]);
      lgp -= 0.5*dS*dS;
    }
    if(Snf[i]<lower[i])
    {
      dS = log(Snf[i]) - log(lower[i]);
      lgp -= 0.5*dS*dS;
    }
  }
  return (lgp);
}

/*
static double logprior_gaussian_model(double *mean, double *sigma, double *Snf, double *spline_f, int spline_n, double *lines_f, int lines_n, dataParams *data)
{
  double x,f;
  double lgp;
  int i,n;

  double flow  = data->flow;
  double Tobs  = data->Tobs;

  lgp = 0.0;

  //Lorentzian model
  for(n=0; n<lines_n; n++)
  {
    f = lines_f[n];
    i = (int)((f-flow)*Tobs);
    x = (mean[i] - Snf[i])/sigma[i];
    lgp -= x*x;


//    printf("    line %i:  f=%g, PSD=%g, , P=%g, sigma=%g, logP=%g\n",n,f,Snf[i],mean[i],fabs(x),lgp);
  }

  //Spline model
  for(n=0; n<spline_n; n++)
  {
    f = spline_f[n];
    i = (int)((f-flow)*Tobs);
    x = (mean[i] - Snf[i])/sigma[i];
    lgp -= x*x;

//    printf("    spline %i:  f=%g, PSD=%g, , P=%g, sigma=%g, logP=%g\n",n,f,Snf[i],mean[i],fabs(x),lgp);
  }

//  system_pause();

  return (0.5*lgp);
}
*/

static double logprior_gaussian(double *mean, double *sigma, double *Snf, int ilow, int ihigh)
{
  double x;
  double lgp;
  int i;

  //leaving off normalizations since they cancel in Hastings ratio
  lgp = 0;
  
  for(i=ilow; i<ihigh; i++)
  {
     x = (mean[i] - Snf[i])/sigma[i];
     lgp -= x*x;
  }
  
  return (0.0*lgp);
}

static double loglike(double *respow, double *Snf, int ncut)
{
  double lgl, x;
  int i;

  // leavimng out the log(2Pi) terms since they cancel in Hastings ratio
  lgl = 0.0;
  for(i=0; i< ncut; i++)
  {
    x = respow[i]/Snf[i];
    lgl -= (x+log(Snf[i]));
  }

  return(lgl);
}

double delta_loglike(double *respow, double *Sn, double *Snx, int imin, int imax)
{
  double lgl, x;
  int i;

  // leaving out the log(2Pi) terms since they cancel in Hastings ratio
  lgl = 0.0;
  for(i=imin; i< imax; i++)
  {
     x = respow[i]/Sn[i]-respow[i]/Snx[i];
     lgl -= (x+log(Sn[i]/Snx[i]));
  }
 
  return(lgl);
}

void spectrum_spline(double *Sn, double *Sbase, double *sfreq, dataParams *data, lorentzianParams *restrict lines, splineParams *restrict spline, int SplineFlag, int SnModel)
{
  int i, j, k, n;
  int istart, istop;

  int nspline     = spline->n;
  double *spoints = spline->points;
  double *sdata   = spline->data;

  double *x,*Stemp;

  n = data->ncut;

  x     = malloc((size_t)(sizeof(double)*(n)));
  Stemp = malloc((size_t)(sizeof(double)*(n)));

  //Interpolate {spoints,sdata} ==> {sfreq,x}
  if(SplineFlag == 1) AkimaSplineGSL(0,n,nspline,spoints,sdata,n,sfreq,x);
  else if(SplineFlag == 0) CubicSplineGSL(0,n,nspline,spoints,sdata,n,sfreq,x);
  
  
  for(i=0; i< n; i++)
  {
    Sbase[i] = exp(x[i]);          // spline base model
    Sn[i] = 0.0;
  }

  for(k=0; k<lines->n; k++)
  {
    // j = lines->larray[k];
    j = k;
    for(i=0; i<n; i++) Stemp[i]=Sn[i];
    full_spectrum_add_or_subtract(Sn, Stemp, Sbase, sfreq, data, lines, j, SnModel, &istart, &istop, 1, 1);
  }

  for(i=0; i< n; i++)
  {
    Sn[i] += Sbase[i];
  }

  free(x);
  free(Stemp);
}

double loglike_pm(double *respow, double *Sn, double *Snx, int ilow, int ihigh)
{
  double lgl, x;
  int i;

  // leavimng out the log(2Pi) terms since they cancel in Hastings ratio
  lgl = 0.0;
  for(i=ilow; i< ihigh; i++)
  {
    x = respow[i]/Sn[i]-respow[i]/Snx[i];
    lgl -= (x+log(Sn[i]/Snx[i]));
  }

  return(lgl);
}



double loglike_single(double *respow, double *Sn, double *Snx, int ilowx, int ihighx, int ilowy, int ihighy)
{
  double lgl, x;
  int i;
  int ilow, ihigh;
  int imid1, imid2;

  ilow = ilowx;
  if(ilowy < ilow) ilow = ilowy;

  if(ilow == ilowx)
  {
    if(ihighx <= ilowy)  // separate regions
    {
      imid1 = ihighx;
      imid2 = ilowy;
      ihigh = ihighy;
    }

    if(ihighx > ilowy) // overlapping regions
    {
      if(ihighx < ihighy)
      {
        imid1 = ihighx;
        imid2 = ihighx;
        ihigh = ihighy;
      }
      else
      {
        imid1 = ilowy;
        imid2 = ilowy;
        ihigh = ihighx;
      }
    }
  }

  if(ilow == ilowy)
  {
    if(ihighy <= ilowx)  // separate regions
    {
      imid1 = ihighy;
      imid2 = ilowx;
      ihigh = ihighx;
    }

    if(ihighy > ilowx) // overlapping regions
    {
      if(ihighy < ihighx)
      {
        imid1 = ihighy;
        imid2 = ihighy;
        ihigh = ihighx;
      }
      else
      {
        imid1 = ilowx;
        imid2 = ilowx;
        ihigh = ihighy;
      }
    }
  }

  // leavimng out the log(2Pi) terms since they cancel in Hastings ratio
  lgl = 0.0;
  for(i=ilow; i< imid1; i++)
  {
    x = respow[i]/Sn[i]-respow[i]/Snx[i];
    lgl -= (x+log(Sn[i]/Snx[i]));
  }
  for(i=imid2; i< ihigh; i++)
  {
    x = respow[i]/Sn[i]-respow[i]/Snx[i];
    lgl -= (x+log(Sn[i]/Snx[i]));
  }

  return(lgl);
}

void full_spectrum_add_or_subtract(double *Snew, double *Sold, double *Sbase, double *sfreq, dataParams *restrict data, lorentzianParams *restrict lines, int ii, int SnModel, int *ilow, int *ihigh, int flag, int lineflag)
{
  int i;
  double dS,f2,f4;
  double deltf;
  double fsq, x, z, deltafmax, spread;
  double amplitude;
  int istart, istop, imid, idelt;

  double A = lines->A[ii];
  double Q = lines->Q[ii];
  double f = lines->f[ii];
  double Q2 = Q*Q;
  
  int    ncut = data->ncut;
  double Tobs = data->Tobs;
  double flow = data->flow;

  // copy over current model
  memcpy(Snew, Sold, ncut*sizeof(double));

  // here we figure out how many frequency bins are needed for the line profile
  imid = (int)((f-flow)*Tobs);
  spread = (1.0e-2*Q);

  if(spread < 50.0) spread = 50.0;  // maximum half-width is f_resonance/20
  deltafmax = f/spread;
  deltf = 8.0*deltafmax;
  idelt = (int)(deltf*Tobs)+1;
  if(A < 10.0*Sbase[imid]) idelt = (int)(20.0*f*Tobs/Q)+1;

  istart = imid-idelt;
  istop = imid+idelt;
  if(istart < 0) istart = 0;
  if(istop > ncut) istop = ncut;

  *ilow  = istart;
  *ihigh = istop;


  // add or remove the old line
  f2=f*f;
  f4=f2*f2;
  amplitude = A*f4;///(f2*Q*Q);
  for(i=istart; i<istop; i++)
  {
    fsq = sfreq[i]*sfreq[i];
    x = fabs(f-sfreq[i]);
    z = 1.0;
    if(x > deltafmax) z = exp(-(x-deltafmax)/deltafmax);
    //dS = z*A*f4/(f2*fsq+Q*Q*(fsq-f2)*(fsq-f2));
    if(i==0)
      dS = 0.0;
    else
    {
      //dS = z*amplitude/(fsq*(fsq-f2)*(fsq-f2));
      dS = z*amplitude/( f2*fsq + Q2*(fsq-f2)*(fsq-f2) );
    }
    switch(flag)
    {
      case 1: //add new line
        if(SnModel == 1 && lineflag == 1) Snew[i] += Sbase[i]*dS;
        else Snew[i] += dS;
        break;
      case -1: //remove line
        if(SnModel == 1 && lineflag == 1) Snew[i] -= Sbase[i]*dS;
        else Snew[i] -= dS;
        break;
      default:
        break;
    }
  }
}

void full_spectrum_single(double *Sn, double *Snx, double *Sbasex, double *sfreq, dataParams *data, lorentzianParams *line_x, lorentzianParams *line_y, int ii, int SnModel,
                          int *ilowx, int *ihighx, int *ilowy, int *ihighy)
{

  double *Stemp = malloc((size_t)(sizeof(double)*(data->ncut)));

  full_spectrum_add_or_subtract(Stemp, Snx, Sbasex, sfreq, data, line_x, ii, SnModel, ilowx, ihighx,-1,1);
  full_spectrum_add_or_subtract(Sn, Stemp,  Sbasex, sfreq, data, line_y, ii, SnModel, ilowy, ihighy, 1,1);

  free(Stemp);
}



void full_spectrum_spline(double *Sline, double *Sbase, double *sfreq, dataParams *restrict data, lorentzianParams *restrict lines, int SnModel)
{
  int i, j, k;
  int istart, istop;

  double *Stemp = malloc((size_t)(sizeof(double)*(data->ncut)));

  for(i=0; i<data->ncut; i++) Sline[i] = 0.0;
  for(k=0; k<lines->n; k++)
  {
      j = k;
    memcpy(Stemp,Sline,data->ncut*sizeof(double));
    full_spectrum_add_or_subtract(Sline, Stemp, Sbase, sfreq, data, lines, j, SnModel, &istart, &istop, 1,0);
  }

  free(Stemp);
}

void AkimaSplineGSL_one(int N, double *x, double *y, double xint, double *yint)
{
  int n;
   
  /* set up GSL akima spline */
  gsl_spline *aspline = gsl_spline_alloc(gsl_interp_akima, N);
  gsl_interp_accel *acc    = gsl_interp_accel_alloc();
  
  /* get derivatives */
  gsl_spline_init(aspline,x,y,N);
    
  /*  GSL akima spline throws errors if
     interpolated points are at end of
     spline control points*/
     
    if     (xint<x[0])
      *yint = y[0];
    else if(xint>x[N-1])
      *yint = y[N-1];
    else
      *yint=gsl_spline_eval(aspline,xint,acc);   
    
  gsl_spline_free (aspline);
  gsl_interp_accel_free (acc);

}

void CubicSplineGSL_one(int N, double *x, double *y, double xint, double *yint)
{
  int n;
   
  /* set up GSL akima spline */
  gsl_spline *cspline = gsl_spline_alloc(gsl_interp_cspline, N);
  gsl_interp_accel *acc    = gsl_interp_accel_alloc();
  
  /* get derivatives */
  gsl_spline_init(cspline,x,y,N);
    
  /*  GSL cubic spline throws errors if
     interpolated points are at end of
     spline control points*/
     
    if     (xint<x[0])
      *yint = y[0];
    else if(xint>x[N-1])
      *yint = y[N-1];
    else
      *yint=gsl_spline_eval(cspline,xint,acc);
  
  gsl_spline_free (cspline);
  gsl_interp_accel_free (acc);

}


void CubicSplineGSL(int imin, int imax, int N, double *x, double *y, int Nint, double *xint, double *yint)
{
  int n;

  /* set up GSL cubic spline */
  gsl_spline       *cspline = gsl_spline_alloc(gsl_interp_cspline, N);
  gsl_interp_accel *acc    = gsl_interp_accel_alloc();

  /* get derivatives */
  gsl_spline_init(cspline,x,y,N);

  /* interpolate */
  for(n=imin; n<imax; n++)
  {
    /*
     GSL cubic spline throws errors if
     interpolated points are at end of
     spline control points
     */
    if     (xint[n]<x[0])
      yint[n] = y[0];
    else if(xint[n]>x[N-1])
      yint[n] = y[N-1];
    else
      yint[n]=gsl_spline_eval(cspline,xint[n],acc);
  }

  gsl_spline_free (cspline);
  gsl_interp_accel_free (acc);

}

void AkimaSplineGSL(int imin, int imax, int N, double *x, double *y, int Nint, double *xint, double *yint)
{
  int n;
   
  /* set up GSL akima spline */
  gsl_spline *aspline = gsl_spline_alloc(gsl_interp_akima, N);
  gsl_interp_accel *acc    = gsl_interp_accel_alloc();
  
  /* get derivatives */
  gsl_spline_init(aspline,x,y,N);
  
  /* interpolate */
  for(n=imin; n<imax; n++)
  {
    /*
     GSL akima spline throws errors if
     interpolated points are at end of
     spline control points
     */
     
    if     (xint[n]<x[0])
      yint[n] = y[0];
    else if(xint[n]>x[N-1])
      yint[n] = y[N-1];
    else
      yint[n]=gsl_spline_eval(aspline,xint[n],acc);
    
  }
 
  gsl_spline_free (aspline);
  gsl_interp_accel_free (acc);

}

void getrangeakima(int iu, int nsy, double *x, dataParams *restrict data, int Nend, int *imin, int *imax)
{
  int Nst;
  Nst = 3;

  if(iu>Nst-1)
  {
      *imin = (int)((floor)((x[iu-Nst]-x[0])*data->Tobs));
  }
  else
  {
     *imin = (int)(0);
  }
  
  if(iu<nsy-Nst)
  {
     *imax = (int)((ceil)((x[iu+Nst]-x[0])*data->Tobs +1));
  }
  else
  {
     *imax = (int)(Nend);
  }
}

void create_dataParams(dataParams *data, double *f, int n,int max_lines)
{

  // length of segment in seconds, this should be read in from the frame file
  data->Tobs = rint(1.0/(f[1]-f[0]));

  //minimum frequency
  data->fmin = f[0];

  //maximum frequency
  data->fmax = f[n-1];

  // frequency resolution
  data->df = 1.0/data->Tobs;

  // sample cadence in Hz, this should be read in from the frame file
  data->cadence = pow(2.0,rint(log((double)(n))/log(2.0))+1.0)/data->Tobs;

  // Nyquist frequency
  data->fny = 2.0/data->cadence;

  // size of segments in Hz
  // If frequency snippets are too large need longer initial runs to get convergence
  data->fstep = 1./data->Tobs;//data->fmin;//16;//((data->fmax-data->fmin)/128);//256./data->Tobs;//30;//FSTEP;//9.0;//30.0;

  // This sets the maximum number of Lorentzian lines.
  data->tmax = max_lines;

  // approximate number of segments
  data->sgmts = (int)((f[n-1]-f[0])/data->fstep)+2;

  //minimum Fourier bin
  data->nmin = (int)(f[0]*data->Tobs);

  // the stencil separation in Hz for the spline model. Going below 2 Hz is dangerous - will fight with line model
  data->fgrid = 4.0;//data->fstep/2.;//15.0;//FSTEP;///4;//4.0;//FSTEP;
}

void create_lorentzianParams(lorentzianParams *lines, int size)
{
  lines->n    = 0;
  lines->size = size;

  lines->larray = malloc((size_t)(sizeof(int)*size));

  lines->f = malloc((size_t)(sizeof(double)*size));
  lines->Q = malloc((size_t)(sizeof(double)*size));
  lines->A = malloc((size_t)(sizeof(double)*size));
}

void copy_lorentzianParams(lorentzianParams *origin, lorentzianParams *copy)
{
  copy->n    = origin->n;
  copy->size = origin->size;

  int n;
 for(n=0; n<origin->n; n++)
  {
    copy->larray[n] = origin->larray[n];

    copy->f[n] = origin->f[n];
    copy->Q[n] = origin->Q[n];
    copy->A[n] = origin->A[n];
  }
}

void destroy_lorentzianParams(lorentzianParams *lines)
{
  free(lines->larray);
  free(lines->f);
  free(lines->Q);
  free(lines->A);
  free(lines);
}

void create_splineParams(splineParams *spline, int size)
{
  spline->n = size;

  spline->data   = malloc((size_t)(sizeof(double)*size));
  spline->points = malloc((size_t)(sizeof(double)*size));
}

void copy_splineParams(splineParams *origin, splineParams *copy)
{
  int n;
  copy->n = origin->n;

  for(n=0; n<origin->n; n++)
  {
    copy->data[n]   = origin->data[n];
    copy->points[n] = origin->points[n];
  }
}

void destroy_splineParams(splineParams *spline)
{
  free(spline->data);
  free(spline->points);
  free(spline);
}

void BayesLineLorentzSplineMCMC(struct BayesLineParams *bayesline, double heat, int steps, int focus, int priorFlag, double *dan, double *fprop, int SplineFlag, int SnModel)
{
  int nsy;
  double logLx, logLy=0.0, logH;
  int ilowx, ihighx, ilowy, ihighy, imin, imax;
  int i, j, k=0, ki=0, ii=0, jj=0, ji, newki, mc, iu;
  int check=0;
  double alpha, alpha1;
  double lSAmax, lSAmin;
  double lQmin, lQmax;
  double lAmin, lAmax;
  int ac0, ac1, ac2;
  int cc0, cc1, cc2;
  double *Sn, *Sbase, *Sbasex, *Sline, *Snx;
  double *xint;
  double e1, e2, e3, e4;
  double x2, x3, x4;
  double s2, s3, s4;
  int typ=0;
  double xsm, pnorm, pmax, fcl, fch, dff;
  double baseav;
  double logpx=0.0, logpy=0.0, logqx=0.0,logqy=0.0,x, y, z, beta;
  double logPsy=0.0,logPsx=0.0;
  double Ac, newfreq, newfreql;
  double *sdatay;
  double *spointsy;
  double mdl, sp, prange;
  double shiftval;
  int *foc;
   
  /* Make local pointers to BayesLineParams structure members */
  dataParams *data           = bayesline->data;
  lorentzianParams *lines_x  = bayesline->lines_x;
  splineParams *spline       = bayesline->spline;
  splineParams *spline_x     = bayesline->spline_x;
  BayesLinePriors *priors    = bayesline->priors;
  double *Snf                = bayesline->Snf;
  double *freq               = bayesline->sfreq;
  double *power              = bayesline->spow;
  gsl_rng *r                 = bayesline->r;

  int ncut = data->ncut;
  int tmax = data->tmax;

  double flow  = data->flow;
  double fhigh = data->fhigh;

  int nspline   = spline->n;

  double *sdata = spline->data;
  double *sdatax = spline_x->data;

  double *spoints  = spline->points;
  double *spointsx = spline_x->points;

  Snx    = malloc((size_t)(sizeof(double)*(ncut)));
  Sn     = malloc((size_t)(sizeof(double)*(ncut)));
  Sbasex = malloc((size_t)(sizeof(double)*(ncut)));
  Sbase  = malloc((size_t)(sizeof(double)*(ncut)));
  Sline  = malloc((size_t)(sizeof(double)*(ncut)));

  sdatay   = malloc((size_t)(sizeof(double)*(nspline)));
  spointsy = malloc((size_t)(sizeof(double)*(nspline)));

  foc     = malloc((size_t)(sizeof(int)*(tmax)));

  // This keeps track of whos who in the Lorentzian model
  // Necessary complication when using delta likelihood
  // calculations that only update a single line
  lorentzianParams *lines_y = malloc(sizeof(lorentzianParams));
  create_lorentzianParams(lines_y,tmax);

  xint = malloc((size_t)(sizeof(double)*(ncut)));
  
  // maxima and minima for the noise spectal slopes and amplitudes
  // uniform priors in slope and log amplitude
  lQmin = log(priors->LQmin);
  lQmax = log(priors->LQmax);
  lAmin = log(priors->LAmin);
  lAmax = log(priors->LAmax);
  // lSAmin = log(priors->SAmin);
  // lSAmax = log(priors->SAmax);
    
  dff = 0.01;  // half-width of frequency focus region (used if focus == 1)

  // this is the fractional error estimate on the noise level
  s2 = 0.5;
  s3 = 0.5;
  s4 = 0.5;

  for(i=0; i<lines_x->n; i++)
  {
    lines_x->larray[i] = i;
    lines_y->larray[i] = i;
  }

 // FILE *temp;
 // temp=fopen("start_psd_bw.dat","w");
 // for(i=0; i<ncut; i++) fprintf(temp,"%lg %lg %lg %lg\n",freq[i],Snf[i],priors->lower[i],priors->upper[i]);
 // fclose(temp);
  
  baseav = 0.0;

  //Interpolate {spointsx,sdatax} ==> {freq,xint}
  if(SplineFlag == 1) AkimaSplineGSL(0,ncut,spline_x->n,spointsx,sdatax,ncut,freq,xint);
  else if(SplineFlag == 0) CubicSplineGSL(0,ncut,spline_x->n,spointsx,sdatax,ncut,freq,xint);
  
  for(i=0; i<ncut; i++)
  {
    Sbase[i] = exp(xint[i]);
    Sbasex[i] = Sbase[i];
    if(HMean ==0) baseav += xint[i];
    if(HMean ==1) baseav += pow(xint[i],-1.0);
  }

  baseav /= (double)(ncut);
  if(HMean ==1) baseav = pow(baseav,-1.0);

  full_spectrum_spline(Sline, Sbase, freq, data, lines_x, SnModel);
  for(i=0; i< ncut; i++) 
  {
      if(SnModel == 1) Sn[i] = Sbase[i]*(1.0+Sline[i]);
      else if(SnModel == 0) Sn[i] = Sbase[i]+Sline[i];
  }

  for(i=0; i<ncut; i++) Snx[i] = Sn[i];
    
  
  if(!bayesline->constantLogLFlag)
    logLx = loglike(power, Sn, ncut);
  else
    logLx = 1.0;

  if(priorFlag==1)
  {
    logPsx = logprior(priors->lower, priors->upper, Snx, 0, ncut);
  }
  if(priorFlag==2)
  {
    logPsx = logprior_gaussian(priors->mean, priors->sigma, Snx, 0, ncut);
  }

  /*
  if(logPsx<0)
  {
    printf("how did PSD come in outside of the prior?\n");
    printf("%lg\n",logPsx);
    FILE *temp=fopen("failed_psd.dat","w");
    for(i=0; i<ncut; i++) fprintf(temp,"%i %lg %lg %lg\n",i,Snx[i],priors->lower[i],priors->upper[i]);
    
    for(i=0; i<ncut; i++)
      {
	if(Snx[i]>priors->upper[i] || Snx[i]<priors->lower[i])
	  {
	    
	    printf("The PSD is outside the prior. Offending point: %i %lg %lg %lg\n",i,Snx[i],priors->lower[i],priors->upper[i]);                                                                      
	  }
      }


    print_line_model(stdout, bayesline);

    exit(1);
  }
   */

    
   
  pmax = -1.0;
  for(i=0; i< ncut; i++)
  {
     if(fprop[i] > pmax) pmax = fprop[i];
  }
  
  // define the focus region (only used if focus flag = 1)
  fcl = freq[k]-dff;
  fch = freq[k]+dff;
  Ac = power[k];
  if(fcl < freq[0]) fcl = freq[0];
  if(fch > freq[ncut-1]) fch = freq[ncut-1];

  ac0 = 0;
  ac1 = 0;
  ac2 = 0;
  cc0 = 1;
  cc1 = 1;
  cc2 = 1;

  if(dfmin == 0.0) shiftval = (double)(gsl_rng_uniform(r));
  else shiftval = (double)(gsl_rng_uniform(r)*(dfmin));
  if(shiftval < (1.0/data->Tobs)) shiftval = 1.0/data->Tobs;
  if(shiftval >=dfmin) shiftval = dfmin;
    
    if(SplineFlag == 0)
    { 
        if(dfmin<2.0) printf("Warning: The spline points are too close.\n Cubic Spline interpolation can result in Wiggles.\n");
    }
    
    //Added
   for(i=0; i< lines_x->n; i++)
   {
      lines_y->Q[i] = lines_x->Q[i];
      lines_y->f[i] = lines_x->f[i];
      lines_y->A[i] = lines_x->A[i];
   }
   lines_y->n = lines_x->n;
    nsy = spline_x->n;
  
    for(i=0; i<spline_x->n; i++)
    {
      sdatay[i] = sdatax[i];
      spointsy[i] = spointsx[i];
    }
  for(mc=0; mc < steps; mc++)
  {
    typ=-1;
    check = 0;

    //copy over current state
    lines_y->n = lines_x->n;
    nsy = spline_x->n;
    
    for(i=0; i< lines_x->n; i++)
    {
      // lines_y->larray[i] = lines_x->larray[i];
      lines_y->Q[i] = lines_x->Q[i];
      lines_y->f[i] = lines_x->f[i];
      lines_y->A[i] = lines_x->A[i];
    }
      
    for(i=0; i<nsy; i++)
    {
      sdatay[i] = sdatax[i];
      spointsy[i] = spointsx[i];
    }
    
    beta = gsl_rng_uniform(r);
    logpx = 0.0; logpy = 0.0; logqx = 0.0; logqy = 0.0;

    if(beta > 0.5)  // update the smooth part of the spectrum
    {

      alpha = gsl_rng_uniform(r);

      logpx = 0.0; logpy = 0.0; logqx = 0.0; logqy = 0.0;
      
      if(alpha > 0.5)  // try a transdimensional move
      {
        
        //decide between adding or removing spline point
        alpha1 = gsl_rng_uniform(r);
          
        if(alpha1 > 0.5)  // try and add a new term
        {
          nsy = spline_x->n+1;
          typ = 5;
        }
        else // try and remove term
        {
          nsy = spline_x->n-1;
          typ = 6;
        }
        
        //Updated birth-death proposal
        if(nsy >=7 && nsy <= nspline-1)
        {
          //Spline death move
          if(typ==6)
          {
            ki=1+(int)(gsl_rng_uniform(r)*(double)(spline_x->n-2)); // pick a term to try and kill - cant be first or last term
            k = 0;
            for(j=0; j<spline_x->n; j++)
            {
              if(j != ki)
              {
                sdatay[k] = sdatax[j];
                spointsy[k] = spointsx[j];
                k++;
              }
            }
            iu = ki;
            newfreq = spointsx[iu];
            
            //Interpolate to find the value it would have, if that point was not there
            if(SplineFlag == 1) AkimaSplineGSL_one(nsy,spointsy,sdatay,spointsx[ki],&mdl);
            else if(SplineFlag == 0) CubicSplineGSL_one(nsy,spointsy,sdatay,spointsx[ki],&mdl);
            
            //Proposal density
            ji = (int)((newfreq-flow)*data->Tobs);
            sp = fabs((log(priors->lower[ji]))*1.0e-3);
            prange = (log(priors->lower[ji]*100.0) - log(priors->lower[ji]));
            
            logqx = rjden(mdl,sdatax[ki],sp,prange);
            logpx = -log(prange);
            logqy = 0.0; logpy = 0.0;
              
          }//end death move

          if(typ==5)
          {
            //Add a new point at a random freq location

            //pick a freq location
             newfreq = flow + (gsl_rng_uniform(r)*((fhigh-flow)));
             spointsy[nsy-1] = newfreq;
             
             if(SplineFlag == 1) AkimaSplineGSL_one(spline_x->n,spointsx,sdatax,spointsy[nsy-1],&mdl);
             else if(SplineFlag == 0) CubicSplineGSL_one(spline_x->n,spointsx,sdatax,spointsy[nsy-1],&mdl);
             
            
             ji = (int)((newfreq-flow)*data->Tobs);
             sp = fabs((log(priors->lower[ji]))*1.0e-3);
             prange = (log(priors->lower[ji]*100.0) - log(priors->lower[ji]));
             sdatay[nsy-1] = rjdraw(mdl,sp,prange,log(priors->lower[ji]),r);
                
             logqy = rjden(mdl, sdatay[nsy-1], sp, prange);
             logpy = -log(prange);
             logqx = 0.0; logpx = 0.0;
              
             gsl_sort2(spointsy, 1, sdatay, 1, nsy);
             for(j=0;j<nsy;j++)
             {
                 if(spointsy[j] == newfreq) iu = j;
             }
              if(iu < 0 || iu >=nsy) printf("Warning: updated point is out of bounds.\n");
          
           }//end birth move
       }
       else check = 1;
    }
     
     //Off grid case
    else if (alpha > 0.3 && spline_x->n >2)//Shift the points laterally
    {
        typ = 7;
        nsy = spline_x->n;
        
        //pick a freq to swap, check if active, add 1/T to that freq, sort and find the label
        
        ki=1+(int)(gsl_rng_uniform(r)*(double)(spline_x->n-2));  // pick a point to swap
        
        alpha1 = gsl_rng_uniform(r);
        if(alpha1 > 0.5)  // shift term to right, seperation between spline points should be greater than minimum spline spacing
        {
            newfreq = spointsx[ki] + shiftval;
            if(spointsx[ki+1] < newfreq) check = 1;
        }
        else // shift term to left
        {
            newfreq = spointsx[ki] - shiftval;
            if(newfreq < spointsx[ki-1])  check = 1;
        }         
        if(check == 0)
        {
            spointsy[ki] = newfreq; // swap it with a new value
            gsl_sort(spointsy,1,nsy);
            ji = (int)((newfreq - flow)*data->Tobs);
            for(j=0; j<nsy; j++)
            {
                if(spointsy[j] == newfreq)
                {
                    sdatay[j] = sdatax[j];
                    iu = j;
                }
            }
        }
      } //end lateral shift move
        
      else  // regular MCMC update
      {
        typ = 4;
        nsy = spline_x->n;
          
        //pick a term to update
        ii=(int)(gsl_rng_uniform(r)*(double)(spline_x->n));

        // use a variety of jump sizes by using a sum of gaussians of different width
        e1 = 0.0005;
        alpha = gsl_rng_uniform(r);
        if(alpha > 0.8)
        {
          e1 = 0.002;
        }
        else if(alpha > 0.6)
        {
          e1 = 0.005;
        }
        else if(alpha > 0.4)
        {
          e1 = 0.05;
        }
        iu = ii;
          
        // propose new value for the selected term
        alpha = gsl_rng_uniform(r);
        if(alpha >0.5)   
        {
            sdatay[ii] = sdatax[ii]+gsl_ran_gaussian(r, e1);
            if(ii >0 && ii< spline_x->n -1) spointsy[ii] = spointsx[ii] + gsl_ran_gaussian(r,e1);
        }
        else
        {
            if(ii >0 && ii< spline_x->n -1) spointsy[ii] = flow + (fhigh-flow)*gsl_rng_uniform(r);
            ji = ((spointsy[ii]-flow)*data->Tobs);
            sdatay[ii] = log(priors->lower[ji]) +(log(priors->lower[ji]*100.0) - log(priors->lower[ji]))*gsl_rng_uniform(r);
        }
         
        newfreq = spointsy[ii];
        logpx = 0.0; logpy = 0.0; logqx=0.0; logqy = 0.0;
          
      }
    
        if(check==0)
        {
            //check minimum spacing between two active points
            for(i =1; i<nsy; i++)
            {
                if( (spointsy[i]-spointsy[i-1])<dfmin) check = 1;  
            }
            
          for(i=0; i<nsy; i++)
          {
              ji = (int)((spointsy[i] - flow)*data->Tobs); //Make sure all active points lie within the prior envelope
              if(sdatay[i] > log(priors->lower[ji]*100.0)) {check = 1; }
              if(sdatay[i] < log(priors->lower[ji]))       {check = 1; }
              if(spointsy[i]<flow || spointsy[i]>fhigh)    {check = 1; }
          }
            
        }
        
    }
    else    // update the line model
    {
      if(tmax == 0) check = 1;
      else
      {
          alpha1 = gsl_rng_uniform(r);
          if(alpha1 > 0.5)  // try a transdimensional move
          {
            alpha = gsl_rng_uniform(r);
            if(alpha > 0.5) // add
            {
                typ = 2;
                lines_y->n = lines_x->n+1;
                
                if(lines_y->n <= tmax)
                {
               
                ii = (int)(lines_y->n-1);
                // gets put into Ny-1 since index runs from 0 to < Ny
                alpha = gsl_rng_uniform(r);
                    
                lines_y->f[ii] = qdraw(fprop, pmax, flow, fhigh, ncut, data->Tobs, r);
                lines_y->A[ii] = exp(lAmin+(lAmax-lAmin)*gsl_rng_uniform(r));
                lines_y->Q[ii] = exp(lQmin+(lQmax-lQmin)*gsl_rng_uniform(r));
                k = (int)((lines_y->f[ii]-flow)*data->Tobs);
                newfreql = lines_y->f[ii];

                logqy = log(fprop[k]);
                logpy = -log((double)(ncut)); 
                logqx = 0.0;
                logpx = 0.0;
                    
                if(SnModel == 0)
                {
                    alpha = gsl_rng_uniform(r);
                    if(alpha < kappa_BL)
                    {
                      y = fabs(gsl_ran_gaussian(r, lAwidth));
                      lines_y->A[ii] = exp(baseav+y);
                    }
                    else
                    {
                      lines_y->A[ii] = exp(lAmin+(lAmax-lAmin)*gsl_rng_uniform(r));
                      y = log(lines_y->A[ii])-baseav;
                    }
                    if(y < 0.0)
                    {
                      z = 0.0;
                    }
                    else
                    {
                      z = 2.0*kappa_BL*gsl_ran_gaussian_pdf(y, lAwidth);
                    }
                    logqy += log((1.0-kappa_BL)/(lAmax-lAmin) + z);
                    logpy += -log((lAmax-lAmin));
                    logqx += 0.0; logpx += 0.0;
                }
                
                if(focus==1) // using focused region (not Markovian - not paying penalty for proposing in such a small region)
                {
                  lines_y->f[i] = fcl+(fch-fcl)*gsl_rng_uniform(r);
                  lines_y->Q[i] = priors->LQmax/10.0;
                  lines_y->A[i] = Ac;

                  logpy += 0.0;
                  logpx += 0.0;
                  logqx += 0.0; logqy += 0.0;
                }
                    
                if(lines_y->A[ii] > priors->LAmax) check = 1;
                if(lines_y->A[ii] < priors->LAmin) check = 1;
                if(lines_y->Q[ii] > priors->LQmax) check = 1;
                if(lines_y->Q[ii] < priors->LQmin) check = 1;
                if(lines_y->f[ii] > fhigh) check = 1; 
                if(lines_y->f[ii] < flow) check = 1;
                    
                 newfreql = lines_y->f[ii];
                 
                    
                }
                else
                {
                    check = 1; 
                }

           } // end add line
           else  // remove
           {
               typ = 3;
               lines_y->n = lines_x->n-1;
               
               if(lines_y->n > 0)
               {
                  // pick one to kill
                   k = (int)((double)(lines_x->n)*gsl_rng_uniform(r));
                   ki = k;
                   // reverse move would add line at this location
                   j = (int)((lines_x->f[k]-flow)*data->Tobs);


                   logqy = 0.0;
                   logpy = 0.0;
                   logqx = log(fprop[j]);
                   logpx = -log((double)(ncut)); 
                   
                   j = 0;
                   for(i=0; i< lines_x->n; i++)
                   {
                       if(i != k)
                       {
                       lines_y->f[j] = lines_x->f[i];
                       lines_y->A[j] = lines_x->A[i];
                       lines_y->Q[j] = lines_x->Q[i];
                       j++;
                       }
                   }
                   if(SnModel == 0)
                   {
                        y = log(lines_x->A[ki])-baseav;
                        if(y < 0.0)
                        {
                          z = 0.0;
                        }
                        else
                        {
                          z = 2.0*kappa_BL*gsl_ran_gaussian_pdf(y, lAwidth);
                        }
                        logqx += log((1.0-kappa_BL)/(lAmax-lAmin) + z);
                        logpx += -log((lAmax-lAmin));
                        logqy += 0.0; logpy += 0.0;
                   }

               }
               else
               {
                   check = 1; 
               }
           }
           
      }
      else  // regular MCMC update
      {
        lines_y->n = lines_x->n;
         
        if(lines_y->n > 0 && lines_y->n <= tmax)
        {
          typ=1;
            
          //pick a term to update
          jj=(int)(gsl_rng_uniform(r)*(double)(lines_x->n));
          //find label of who is geting updated
          ii = jj;

          if(focus == 1)
          {
            // find if any lines are in the focus region
            j = 0;
            for(i=0; i< lines_x->n; i++)
            {
                k = i;
              if(lines_x->f[k] > fcl && lines_x->f[k] < fch)
              {
                foc[j] = k;
                j++;
              }
            }

            x = 0.0;
            if(j > 0)  // some lines are currently in the focus region
            {
              x = 0.8;
              jj=(int)(gsl_rng_uniform(r)*(double)(j));
              //find label of who is getting updated in the focus region
              ii = foc[jj];
            }

          }
          else
          {
            x = 0.8;
          }
            
          if(SnModel== 1) x = 2.0;

          alpha = gsl_rng_uniform(r);
          if(alpha > x)
          {
             
            // here we try and move an exisiting line to a totally new location
            if(focus != 1)
            {
              lines_y->f[ii] = qdraw(fprop, pmax, flow, fhigh, ncut, data->Tobs, r);
              logqy = lprop(lines_y->f[ii], fprop, data);
              logpy = lprop(lines_x->f[ii], fprop, data);
              logpx = 0.0; logqx = 0.0;

              lines_y->Q[ii] = exp(lQmin+(lQmax-lQmin)*gsl_rng_uniform(r));
              alpha = gsl_rng_uniform(r);
              if(alpha < kappa_BL)
              {
                y = fabs(gsl_ran_gaussian(r, lAwidth));
                lines_y->A[ii] = exp(baseav+y);
              }
              else
              {
                lines_y->A[ii] = exp(lAmin+(lAmax-lAmin)*gsl_rng_uniform(r));
                y = log(lines_y->A[ii])-baseav;
              }
              if(y < 0.0)
              {
                z = 0.0;
              }
              else
              {
                z = 2.0*kappa_BL*gsl_ran_gaussian_pdf(y, lAwidth);
              }
              logqy += log((1.0-kappa_BL)/(lAmax-lAmin) + z);
              logpy += -log((lAmax-lAmin));
              logpx += 0.0; logqx += 0.0;

            }
            else  // using focused region (not Markovian - not paying penalty for proposing in such a small region)
            {
              lines_y->f[ii] = fcl+(fch-fcl)*gsl_rng_uniform(r);
              lines_y->Q[ii] = priors->LQmax/10.0;
              lines_y->A[ii] = Ac;

              logpy = 0.0;
              logpx = 0.0;
              logqx = 0.0; logqy = 0.0;
            }
            
            typ = 0;
          }
          else
          {
            typ = 1;
            alpha = gsl_rng_uniform(r);

            if     (alpha > 0.9) beta = 1.0e+1;
            else if(alpha > 0.6) beta = 1.0e+0;
            else if(alpha > 0.3) beta = 1.0e-1;
            else                 beta = 1.0e-2;

            e2 = beta*s2;
            e3 = beta*s3;
            e4 = beta*s4;
            
            x2 = gsl_ran_gaussian(r, e2);
            x3 = gsl_ran_gaussian(r, e3);
            x4 = gsl_ran_gaussian(r, e4);
       
            lines_y->A[ii] = lines_x->A[ii]*exp(x3);
            lines_y->Q[ii] = lines_x->Q[ii]*exp(x4);
            lines_y->f[ii] = lines_x->f[ii]+x2;
             
            logpx = 0.0; logpy = 0.0;
            logqx = 0.0; logqy = 0.0;
          }
          newfreql = lines_y->f[ii];
         
          if(lines_y->A[ii] > priors->LAmax || lines_y->A[ii] < priors->LAmin)  check = 1; 
          if(lines_y->f[ii] < flow || lines_y->f[ii] > fhigh)                   check = 1;
          if(lines_y->Q[ii] < priors->LQmin || lines_y->Q[ii] > priors->LQmax)  check = 1;
          }
          else check = 1;
      }

      }
    }
      
    //If line parameters satisfy priors, continue with MCMC update
    if(check == 0)
    {
      if(typ == 0) cc0++;
      if(typ == 1) cc1++;
      if(typ == 4) cc2++;

      if(typ > 3)  // delta updates
      {
        if(nsy>=7)
        {
          if(!bayesline->constantLogLFlag)
          {
              imin = 0; imax = ncut;
              if(SplineFlag == 0)
              {
                //Interpolate in that region {spointsy, sdatay} ==> {freq,xint}
                CubicSplineGSL(imin,imax,nsy,spointsy,sdatay,ncut,freq,xint);
              }
              if(SplineFlag == 1)
              {
                 //Akima Spline
                 for(i=0; i<ncut; i++) { Sbase[i] = Sbasex[i]; Sn[i] = Snx[i];}
                 // if control point iu is updated, region between (iu-3) and (iu+3) is impacted. Care needs to be taken at boundary points
                 getrangeakima(iu,nsy,spointsy,data,ncut,&imin,&imax);

                 //Interpolate in that region {spointsy, sdatay} ==> {freq,xint}
                 AkimaSplineGSL(imin,imax,nsy,spointsy,sdatay,ncut,freq,xint);
                 
              }  

              for(i=imin;i<imax; i++) Sbase[i] = exp(xint[i]);

              full_spectrum_spline(Sline, Sbase, freq, data, lines_y, SnModel);
              for(i=imin; i<imax; i++) 
              {
                  if(SnModel == 1) Sn[i] = Sbase[i]*(1.0 + Sline[i]);
                  else if(SnModel == 0) Sn[i] = Sbase[i] + Sline[i];
              }
              logLy = logLx + delta_loglike(power, Sn, Snx, imin, imax);
          }
          else logLy = 1.0; 
        }
        else logLy = -1e60;
      }

      if(typ == 1 || typ == 0)  // fixed dimension MCMC of line ii
      {
        if(!bayesline->constantLogLFlag)
        {
            full_spectrum_single(Sn, Snx, Sbasex, freq, data, lines_x, lines_y, ii, SnModel, &ilowx, &ihighx, &ilowy, &ihighy);
            logLy = logLx + loglike_single(power, Sn, Snx, ilowx, ihighx, ilowy, ihighy);
        }
        else logLy = 1.0;
      }

      if(typ == 2)  // add new line with label ii
      {
        if(!bayesline->constantLogLFlag)
        {
            full_spectrum_add_or_subtract(Sn, Snx, Sbasex, freq, data, lines_y, ii, SnModel, &ilowy, &ihighy,  1, 1);
            logLy = logLx + loglike_pm(power, Sn, Snx, ilowy, ihighy);
        }
        else logLy = 1.0;
      }

      if(typ == 3)  // remove line with label ki
      {
        if(!bayesline->constantLogLFlag)
        {
             full_spectrum_add_or_subtract(Sn, Snx, Sbasex, freq, data, lines_x, ki, SnModel, &ilowx, &ihighx, -1, 1);
             logLy = logLx + loglike_pm(power, Sn, Snx, ilowx, ihighx);
        }
        else logLy = 1.0;
      }


      // prior on line number e(-ZETA * n).  (this is a prior, not a proposal)
      // PLEASE KEEP THIS ON (Important for Evidence)
      // effectively an SNR cut on lines
      logpy -= ZETA*(double)(lines_y->n);
      logpx -= ZETA*(double)(lines_x->n);

      //logPsy = logprior(priors->invsigma, priors->mean, Sn, 0, ncut);
      //if(priorFlag)logPsy = logprior(priors->sigma, priors->mean, Sn, 0, ncut);
      if(priorFlag==1)
      {
        logPsy = logprior(priors->lower, priors->upper, Sn, 0, ncut);
        //logPsy = logprior_gaussian_model(priors->mean, priors->sigma, Sn, spointsy, nsy, lines_y->f, lines_y->n, data);
      }
      if(priorFlag==2)
      {
        logPsy = logprior_gaussian(priors->mean, priors->sigma, Sn, 0, ncut);
      }
  
      // if(typ>3) printf("typ %d iu %d check %d log %f %f logp %f %f logq %f %f\n", typ, iu, check, logLx, logLy, logpx, logpy, logqx, logqy);
      logH  = (logLy - logLx)*heat +logpy-logqy-logpx+logqx;
      if(priorFlag!=0) logH += logPsy - logPsx;

      alpha = log(gsl_rng_uniform(r));
     
      if(logH > alpha)
      {
        if(typ == 0) ac0++;
        if(typ == 1) ac1++;
        if(typ == 4) ac2++;
        logLx = logLy;
         
        
        //if(priorFlag!=0)
        logPsx = logPsy;
        lines_x->n = lines_y->n;
        spline_x->n = nsy;
        for(i=0; i< ncut; i++)
        {
          Snx[i] = Sn[i];
          if(typ > 3) Sbasex[i] = Sbase[i];
        }
        for(i=0; i< lines_x->n; i++)
        {
          lines_x->A[i] = lines_y->A[i];
          lines_x->f[i] = lines_y->f[i];
          lines_x->Q[i] = lines_y->Q[i];
        }
        for(i=0; i<spline_x->n; i++)
        {
          sdatax[i] = sdatay[i];
          spointsx[i] = spointsy[i];
        }
      }

    }//end prior check
     

    //Every 1000 steps update focus region
//     if(mc%1000 == 0)
//     {
//         printf("Does it come here mc %d\n", mc);
//       pmax = -1.0;
//       for(i=0; i< ncut; i++)
//       {
//         x = power[i]/Snx[i];
//         if(x > pmax)
//         {
//           pmax = x;
//           k = i;
//         }
//       }

//       // define the focus region (only used if focus flag = 1)
//       fcl = freq[k]-dff;
//       fch = freq[k]+dff;
//       Ac = power[k];
//       if(fcl < freq[0]) fcl = freq[0];
//       if(fch > freq[ncut-1]) fch = freq[ncut-1];

//       //if(focus==1)fprintf(stdout,"Focusing on [%f %f] Hz...\n", fcl, fch);

//       // set up proposal for frequency jumps
//       xsm =0.0;
//       baseav = 0.0;
//       for(i=0; i< ncut; i++)
//       {
//         x = power[i]/Snx[i];
//         if(x < 16.0) x = 1.0;
//         if(x >= 16.0) x = 100.0;
//         fprop[i] = x;
//         xsm += x;
//         if(HMean ==0) baseav += log(Sbasex[i]);
//         if(HMean ==1) baseav += pow(log(Sbasex[i]),-1.0);
//       }
//       baseav /= (double)(ncut);
//       if(HMean ==1) baseav = pow(baseav,-1.0);


//       pmax = -1.0;
//       for(i=0; i< ncut; i++)
//       {
//         fprop[i] /= xsm;
//         if(fprop[i] > pmax) pmax = fprop[i];
//       }

//     }//end focus update
  
  }//End MCMC loop
    
  //Interpolate {spointsx,sdatax} ==> {freq,xint}
  if(SplineFlag == 1) AkimaSplineGSL(0,ncut,spline_x->n,spointsx,sdatax,ncut,freq,xint);
  else if(SplineFlag == 0) CubicSplineGSL(0,ncut,spline_x->n,spointsx,sdatax,ncut,freq,xint);
  
  
  for(i=0; i< ncut; i++) Sbase[i] = exp(xint[i]);

  full_spectrum_spline(Sline, Sbase, freq, data, lines_x, SnModel);
  for(i=0; i< ncut; i++)
  {
    if(SnModel == 1) Sn[i] = Sbase[i]*(1.0+Sline[i]);
    else if(SnModel == 0) Sn[i] = Sbase[i]+Sline[i];
      
    bayesline->Sbase[i] = Sbase[i];
    bayesline->Sline[i] = Sline[i];
  }

  // return updated spectral model
  for(i=0; i< ncut; i++) Snf[i] = Snx[i];

  // re-map the array to 0..mx ordering
  for(i=0; i< lines_x->n; i++)
  {
    k = i;
    lines_y->f[i] = lines_x->f[k];
    lines_y->A[i] = lines_x->A[k];
    lines_y->Q[i] = lines_x->Q[k];
    lines_y->larray[i] = i;
  }
  
  // return the last value of the chain
  for(i=0; i< lines_x->n; i++)
  {
    lines_x->f[i] = lines_y->f[i];
    lines_x->A[i] = lines_y->A[i];
    lines_x->Q[i] = lines_y->Q[i];
    lines_x->larray[i] = i;
  }
  
  // check for outliers
  pmax = -1.0;
  for(i=0; i< ncut; i++)
  {
    x = power[i]/Snx[i];
    if(x > pmax) pmax = x;
  }
  
  *dan = pmax;

  free(foc);
  free(xint);
  free(Snx);
  free(Sn);
  free(Sbase);
  free(Sbasex);
  free(Sline);
  free(sdatay);
  free(spointsy);
    
  destroy_lorentzianParams(lines_y);
  
}

void BayesLineFree(struct BayesLineParams *bptr)
{
  free(bptr->data);

  // storage for line model for a segment. These get updated and passed back from the fitting routine
  destroy_lorentzianParams(bptr->lines_x);

  destroy_splineParams(bptr->spline);
  destroy_splineParams(bptr->spline_x);

  free(bptr->fa);
  free(bptr->Sna);
  free(bptr->freq);
  free(bptr->power);
  free(bptr->Snf);
  free(bptr->Sbase);
  free(bptr->Sline);

  free(bptr->spow);
  free(bptr->sfreq);

  /* set up GSL random number generator */
  gsl_rng_free(bptr->r);

  
  free(bptr);

}

void BayesLineSetup(struct BayesLineParams *bptr, double *freqData, double fmin, double fmax, double deltaT, double Tobs)
{
  int i;
  int n;
  int imin, imax;
  int j;
  double reFreq,imFreq;
  int max_lines=bptr->maxBLLines;

  bptr->data = malloc(sizeof(dataParams));

  bptr->lines_x    = malloc(sizeof(lorentzianParams));

  bptr->spline   = malloc(sizeof(splineParams));
  bptr->spline_x = malloc(sizeof(splineParams));

  bptr->TwoDeltaT = deltaT*2.0;
  
  /* set up GSL random number generator */
  const gsl_rng_type *T = gsl_rng_default;
  bptr->r = gsl_rng_alloc (T);
  gsl_rng_env_setup();

  imin = (int)(fmin*Tobs);
  imax = (int)(fmax*Tobs)+1;
  n    = imax-imin;

  bptr->freq  = malloc((size_t)(sizeof(double)*(n)));
  bptr->power = malloc((size_t)(sizeof(double)*(n)));
  bptr->Snf   = malloc((size_t)(sizeof(double)*(n)));
  bptr->Sbase = malloc((size_t)(sizeof(double)*(n)));
  bptr->Sline = malloc((size_t)(sizeof(double)*(n)));

  for(i=0; i<n; i++)
  {
    j = i+imin;
    bptr->freq[i] = (double)(j)/Tobs;

    reFreq = freqData[2*j];
    imFreq = freqData[2*j+1];

    bptr->power[i] = (reFreq*reFreq+imFreq*imFreq);
  }

  // storage for data meta parameters
  create_dataParams(bptr->data,bptr->freq,n,max_lines);

  // storage for master line model
  create_lorentzianParams(bptr->lines_x,bptr->data->tmax);

  // start with a single line. This number gets updated and passed back
  bptr->lines_x->n = 1;

  //start with ~4 Hz resolution for the spline
  int nspline = (int)(bptr->data->fstep/bptr->data->fgrid)+2;
  int smodn   = nspline*bptr->data->sgmts;

  bptr->fa  = malloc((size_t)(sizeof(double)*(smodn+1)));
  bptr->Sna = malloc((size_t)(sizeof(double)*(smodn+1)));


  //start with ~4 Hz resolution for the spline
  nspline = (int)((bptr->data->fmax-bptr->data->fmin)/bptr->data->fgrid)+2;
  //nspline = (int)(fmax*Tobs);//(int)((bptr->data->fmax-bptr->data->fmin)/bptr->data->fgrid)+2;

  create_splineParams(bptr->spline,nspline);

  create_splineParams(bptr->spline_x,nspline);


  // Re-set dataParams structure to use full dataset
  bptr->data->flow  = bptr->data->fmin;
  bptr->data->fhigh = bptr->data->fmax;
  imax  = (int)(floor(bptr->data->fhigh*bptr->data->Tobs));
  imin  = (int)(floor(bptr->data->flow*bptr->data->Tobs));
  bptr->data->ncut  = imax-imin;

  /*
   The spow and sfreq arrays hold the frequency and power of the full section of data to be whitened
   The model parameters are the number of Lorentzians, nx, and their frequency ff, amplitude AA and
   quality factor QQ, as well as the number of terms in the power law fit to the smooth part of the
   spectrum, segs, and their amplitudes at 100 Hz, SA, and their spectral slopes SP. The maximum number
   of terms in the Lorentzian model is tmax (around 200-300 should cover most spectra), and the
   maximum number of terms in the power law fit is segmax (aound 12-20 should be enough). When called
   from another MCMC code, the LorentzMCMC code needs to be passed the most recent values for the
   noise model. Arrays to hold them have to be declared somewhere in advance. The updated values are
   passed back. The first entry in the call to LoretnzMCMC are the number of iterations. It probably
   makes sense to do 100 or so each time it is called.
   */

  bptr->spow  = malloc((size_t)(sizeof(double)*(bptr->data->ncut)));
  bptr->sfreq = malloc((size_t)(sizeof(double)*(bptr->data->ncut)));

}

void BayesLineInitialize(struct BayesLineParams *bayesline)
{

  int i;
  int imin,imax;
  int j;


  int jj = 0;

  /******************************************************************************/
  /*                                                                            */
  /*  Rapid non-Markovian fit over small bandwidth blocks of data               */
  /*                                                                            */
  /******************************************************************************/

  if(bayesline->lines_x->n < 1 ) bayesline->lines_x->n = 1;

  // Re-set dataParams structure to use full dataset
  bayesline->data->flow  = bayesline->data->fmin;
  imax  = (int)(floor(bayesline->data->fhigh*bayesline->data->Tobs));
  imin  = (int)(floor(bayesline->data->flow*bayesline->data->Tobs));
  bayesline->data->ncut  = imax-imin;

  /******************************************************************************/
  /*                                                                            */
  /*  Full-data spline fit (holding lines fixed from search phase)              */
  /*                                                                            */
  /******************************************************************************/

  int k;
  double mdn;
  double x,z;

  /* Make local pointers to BayesLineParams structure members */
  dataParams *data             = bayesline->data;
  splineParams *spline         = bayesline->spline;
  double *Sna                  = bayesline->Sna;
  double *fa                   = bayesline->fa;


  //initialize spline grid & PSD values
  k=0;
  for(j=0; j<bayesline->spline->n; j++)
  {
    x = data->fmin+(data->fhigh-data->fmin)*(double)(j)/(double)(bayesline->spline->n-1);

    if     (x <= fa[0])    z = Sna[0];
    else if(x >= fa[jj-1]) z = Sna[jj-1];
    else
    {
      i=k-10;
      mdn = 0.0;
      do
      {
        if(i>=0) mdn = fa[i];
        i++;
      } while(x > mdn);

      k = i;
      z = Sna[i];
    }

    spline->points[j] = x;
    spline->data[j]   = z;
  }

  for(j=0; j<bayesline->spline->n; j++)
  {
    bayesline->spline_x->points[j] = bayesline->spline->points[j];
    bayesline->spline_x->data[j]   = bayesline->spline->data[j];
  }

  bayesline->data->flow  = bayesline->data->fmin;

  imin  = (int)(floor(bayesline->data->flow*bayesline->data->Tobs));

  for(i=0; i< bayesline->data->ncut; i++)
  {
    j = i + imin - bayesline->data->nmin;
    bayesline->spow[i] = bayesline->power[j];
    bayesline->sfreq[i] = bayesline->freq[j];
  }

}

void BayesLineRJMCMC(struct BayesLineParams *bayesline, double *freqData, double *psd, double *invpsd, double *splinePSD, int N, int cycle, double beta, int priorFlag, double *fprop, int SplineFlag, int SnModel)
{
  int i,j;
  int imin,imax;
  double reFreq,imFreq;
  double dan;

  //at frequencies below fmin output SAmax (killing lower frequencies in any future inner products)
  imax = (int)(floor(bayesline->data->fmax * bayesline->data->Tobs));
  imin = (int)(floor(bayesline->data->fmin * bayesline->data->Tobs));

  for(i=0; i< bayesline->data->ncut; i++)
  {
    j = i + imin;

    reFreq = freqData[2*j];
    imFreq = freqData[2*j+1];

    bayesline->spow[i] = (reFreq*reFreq+imFreq*imFreq);
  }
  
  BayesLineLorentzSplineMCMC(bayesline, beta, cycle, 0, priorFlag, &dan, fprop, SplineFlag, SnModel);

  /******************************************************************************/
  /*                                                                            */
  /*  Output PSD in format for BayesWave                                        */
  /*                                                                            */
  /******************************************************************************/

  for(i=0; i<N/2; i++)
  {
    if(i>=imin && i<imax)
    {
      psd[i] = bayesline->Snf[i-imin];
      splinePSD[i] = bayesline->Sbase[i-imin];
    }
    else
    {
      psd[i] = 1.0;
      splinePSD[i] = 1.0;
    }
    invpsd[i] = 1./psd[i];
  }
}

double rjdraw(double model, double sp, double prange, double pmin, gsl_rng *r)
{
    double x;
    double alpha;
        
    alpha = gsl_rng_uniform(r);
        
    if(alpha < ufrac)  // uniform draw
    {
        x = pmin + prange *gsl_rng_uniform(r);
    }
    else
    {
        x = model + sp*gsl_ran_gaussian(r,1.0);
    }
    return x;
}

double rjden(double model, double ref, double sp, double prange)
{
    double x, u;

    u = (ref-model)/sp;
    x = log( ufrac/prange + (1.0-ufrac)/(sp*sqrt(TPI))*exp(-u*u/2.0));
    return x;
}

void copy_bayesline_params(struct BayesLineParams *origin, struct BayesLineParams *copy)
{
  copy->data->tmax = origin->data->tmax;

  int i;
  for(i=0; i<origin->data->ncut; i++)
  {
    copy->spow[i]  = origin->spow[i];
    copy->sfreq[i] = origin->sfreq[i];
  }

  int imax = (int)(floor(origin->data->fmax  * origin->data->Tobs));
  int imin = (int)(floor(origin->data->fmin  * origin->data->Tobs));
  for(i=0; i<imax-imin; i++)
  {
    copy->Snf[i]    = origin->Snf[i];
    copy->spow[i]   = origin->spow[i];
    copy->sfreq[i]  = origin->sfreq[i];
    copy->Sbase[i]  = origin->Sbase[i];
    copy->Sline[i]  = origin->Sline[i];
  }

  copy_splineParams(origin->spline, copy->spline);
  copy_splineParams(origin->spline_x,copy->spline_x);
  copy_lorentzianParams(origin->lines_x, copy->lines_x);

}

void print_line_model(FILE *fptr, struct BayesLineParams *bayesline)
{
  int j;
  
  fprintf(fptr,"%i ", bayesline->lines_x->n);
  for(j=0; j< bayesline->lines_x->n; j++) fprintf(fptr,"%lg %lg %lg ", bayesline->lines_x->f[j], bayesline->lines_x->A[j], bayesline->lines_x->Q[j]);
  fprintf(fptr,"\n");

}
void print_spline_model(FILE *fptr, struct BayesLineParams *bayesline)
{
  int j;

  fprintf(fptr,"%i ", bayesline->spline_x->n);
  for(j=0; j<bayesline->spline_x->n; j++) fprintf(fptr,"%.4lf %lg ",bayesline->spline_x->points[j],bayesline->spline_x->data[j]);
  fprintf(fptr,"\n");
}

void parse_line_model(FILE *fptr, struct BayesLineParams *bayesline)
{
  int j;

  fscanf(fptr,"%i",&bayesline->lines_x->n);
  for(j=0; j< bayesline->lines_x->n; j++) fscanf(fptr,"%lg %lg %lg",&bayesline->lines_x->f[j],&bayesline->lines_x->A[j],&bayesline->lines_x->Q[j]);
}
void parse_spline_model(FILE *fptr, struct BayesLineParams *bayesline)
{
  int j;

  fscanf(fptr,"%i",&bayesline->spline_x->n);
  for(j=0; j<bayesline->spline_x->n; j++)fscanf(fptr,"%lg %lg",&bayesline->spline_x->points[j],&bayesline->spline_x->data[j]);
}


/******************************************************************************/
/*                                                                            */
/*  New for O3 BayesLine burn in functions                                    */
/*                                                                            */
/******************************************************************************/

#include <gsl/gsl_statistics.h>
#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_halfcomplex.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_sort.h>

#define Qs 8.0          // Q used in the glitch cleaning for spectral estimation
#define fsp 4.0         // spacing of spline points in Hz
#define TPI 6.2831853071795862319959269370884
#define RTPI 1.772453850905516                    // sqrt(Pi)
#define sthresh 10.0
#define warm 6.0
#define LN2 0.6931471805599453                 // ln 2


#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif

static int *int_vector(int N)
{
    return malloc( (N+1) * sizeof(int) );
}

static void free_int_vector(int *v)
{
    free(v);
}

static double *double_vector(int N)
{
  return malloc( (N+1) * sizeof(double) );
}

static void free_double_vector(double *v)
{
  free(v);
}

static double **double_matrix(int N, int M)
{
  int i;
  double **m = malloc( (N+1) * sizeof(double *));
  
  for(i=0; i<N+1; i++)
  {
    m[i] = malloc( (M+1) * sizeof(double));
  }
  
  return m;
}

static void free_double_matrix(double **m, int N)
{
  int i;
  for(i=0; i<N+1; i++) free_double_vector(m[i]);
  free(m);
}


static void tukey(double *data, double alpha, int N)
{
  int i, imin, imax;
  double filter;
  
  imin = (int)(alpha*(double)(N-1)/2.0);
  imax = (int)((double)(N-1)*(1.0-alpha/2.0));
  
    int Nwin = N-imax;

    for(i=0; i< N; i++)
  {
    filter = 1.0;
    if(i<imin) filter = 0.5*(1.0+cos(M_PI*( (double)(i)/(double)(imin)-1.0 )));
    if(i>imax) filter = 0.5*(1.0+cos(M_PI*( (double)(i-imax)/(double)(Nwin))));
    data[i] *= filter;
  }
  
}

static void tukey_scale(double *s1, double *s2, double alpha, int N)
{
  int i, imin, imax;
  double x1, x2;
  double filter;
  
  imin = (int)(alpha*(double)(N-1)/2.0);
  imax = (int)((double)(N-1)*(1.0-alpha/2.0));
  
    int Nwin = N-imax;

  x1 = 0.0;
  x2 = 0.0;
  for(i=0; i< N; i++)
  {
    filter = 1.0;
    if(i<imin) filter = 0.5*(1.0+cos(M_PI*( (double)(i)/(double)(imin)-1.0 )));
    if(i>imax) filter = 0.5*(1.0+cos(M_PI*( (double)(i-imax)/(double)(Nwin))));
    x1 += filter;
    x2 += filter*filter;
  }
  x1 /= (double)(N);
  x2 /= (double)(N);
  
  *s1 = x1;
  *s2 = sqrt(x2);
  
}

static void phase_blind_time_shift(double *corr, double *corrf, double *data1, double *data2, int n)
{
  int nb2, i, l, k;
  
  nb2 = n / 2;
  
  corr[0] = 0.0;
  corrf[0] = 0.0;
  corr[nb2] = 0.0;
  corrf[nb2] = 0.0;
  
  for (i=1; i < nb2; i++)
  {
    l=i;
    k=n-i;
    
    corr[l]  = (data1[l]*data2[l] + data1[k]*data2[k]);
    corr[k]  = (data1[k]*data2[l] - data1[l]*data2[k]);
    corrf[l] = corr[k];
    corrf[k] = -corr[l];
  }
  
  gsl_fft_halfcomplex_radix2_inverse(corr, 1, n);
  gsl_fft_halfcomplex_radix2_inverse(corrf, 1, n);
  
  
}

static void SineGaussianC(double *hs, double *sigpar, double Tobs, int NMAX)
{
  double f0, Q, sf;
  double fmax, fmin, fac;
  double f;
  double tau, dt;
  int imin, imax;
  
  int i;
  
  // Torrence and Compo
  
  f0 = sigpar[1];
  Q = sigpar[2];
  
  tau = Q/(TPI*f0);
  
  dt = Tobs/(double)(NMAX);
  
  fmax = f0 + 3.0/tau;  // no point evaluating waveform past this time (many efolds down)
  fmin = f0 - 3.0/tau;  // no point evaluating waveform before this time (many efolds down)
  
  fac = sqrt(sqrt(2.0)*M_PI*tau/dt);
  
  i = (int)(f0*Tobs);
  imin = (int)(fmin*Tobs);
  imax = (int)(fmax*Tobs);
  if(imax - imin < 10)
  {
    imin = i-5;
    imax = i+5;
  }
  
  if(imin < 0) imin = 0;
  if(imax > NMAX/2) imax = NMAX/2;
  
  hs[0] = 0.0;
  hs[NMAX/2] = 0.0;
  
  for(i = 1; i < NMAX/2; i++)
  {
    hs[i] = 0.0;
    hs[NMAX-i] = 0.0;
    
    if(i > imin && i < imax)
    {
      f = (double)(i)/Tobs;
      sf = fac*exp(-M_PI*M_PI*tau*tau*(f-f0)*(f-f0));
      hs[i] = sf;
      hs[NMAX-i] = 0.0;
    }
    
  }
  
  
}

static double f_nwip(double *a, double *b, int n)
{
  int i, j, k;
  double arg, product;
  double ReA, ReB, ImA, ImB;
  
  arg = 0.0;
  for(i=1; i<n/2; i++)
  {
    j = i;
    k = n-1;
    ReA = a[j]; ImA = a[k];
    ReB = b[j]; ImB = b[k];
    product = ReA*ReB + ImA*ImB;
    arg += product;
  }
  
  return(arg);
  
}

static void TransformC(double *a, double *freqs, double **tf, double **tfR, double **tfI, double Q, double Tobs, int n, int m, int Nthreads)
{
    double f;
    double bmag;
    
    // [0] t0 [1] f0 [2] Q [3] Amp [4] phi
    
    
    double fix = sqrt((double)(n/2));
    

#pragma omp parallel for num_threads(Nthreads) default(none) private(fix, f, bmag) shared(a, freqs, tf, tfR, tfI, Q, Tobs, n, m, Nthreads)
    for(int j = 0; j < m; j+=1){
        //Only one thread runs this section

        double *params= double_vector(5);
        double *AC=double_vector(n-1);
        double *AF=double_vector(n-1);
        double *b = double_vector(n-1);

        params[0] = 0.0;
        params[2] = Q;
        params[3] = 1.0;
        params[4] = 0.0;



       f = freqs[j];

       params[1] = f;

       SineGaussianC(b, params, Tobs, n);

       bmag = sqrt(f_nwip(b, b, n)/(double)n);

       bmag /= fix;

       //printf("%f %f\n", f, bmag);

       phase_blind_time_shift(AC, AF, a, b, n);


       for(int i = 0; i < n; i++)
       {

           tfR[j][i] = AC[i]/bmag;
           tfI[j][i] = AF[i]/bmag;
           tf[j][i] = tfR[j][i]*tfR[j][i]+tfI[j][i]*tfI[j][i];
       }


        free_double_vector(AC);
        free_double_vector(AF);
        free_double_vector(b);
        free_double_vector(params);
    }
}

static double Getscale(double *freqs, double Q, double Tobs, double fmax, int n, int m, int Nthreads)
{
  double *data, *intime, *ref, **tfR, **tfI, **tf;
  double f, t0, delt, t, x, dt;
  double scale, sqf;
  int i, j;
  
  data   = double_vector(n-1);
  ref    = double_vector(n-1);
  intime = double_vector(n-1);
  
  tf  = double_matrix(m-1,n-1);
  tfR = double_matrix(m-1,n-1);
  tfI = double_matrix(m-1,n-1);
  
  f = fmax/4.0;
  t0 = Tobs/2.0;
  delt = Tobs/8.0;
  dt = Tobs/(double)(n);
  
  //out = fopen("packet.dat","w");
  for(i=0; i< n; i++)
  {
    t = (double)(i)*dt;
    x = (t-t0)/delt;
    x = x*x/2.0;
    data[i] = cos(TPI*t*f)*exp(-x);
    ref[i] = data[i];
    //fprintf(out,"%e %e\n", t, data[i]);
  }
  // fclose(out);
  
  
  gsl_fft_real_radix2_transform(data, 1, n);

  TransformC(data, freqs, tf, tfR, tfI, Q, Tobs, n, m, Nthreads);
  
  
  for(i = 0; i < n; i++) intime[i] = 0.0;
  
  for(j = 0; j < m; j++)
  {
    
    f = freqs[j];
    
    
    sqf = sqrt(f);
    
    for(i = 0; i < n; i++)
    {
      intime[i] += sqf*tfR[j][i];
    }
    
  }
  
  x = 0.0;
  j = 0;
  // out = fopen("testtime.dat","w");
  for(i=0; i< n; i++)
  {
    // fprintf(out,"%e %e %e\n",times[i], intime[i], ref[i]);
    
    if(fabs(ref[i]) > 0.01)
    {
      j++;
      x += intime[i]/ref[i];
    }
  }
  //fclose(out);
  
  x /= sqrt((double)(2*n));
  
  scale = (double)j/x;
  
  // printf("scaling = %e %e\n", x/(double)j, (double)j/x);
  
  free_double_vector(data);
  free_double_vector(ref);
  free_double_vector(intime);
  free_double_matrix(tf, m-1);
  free_double_matrix(tfR,m-1);
  free_double_matrix(tfI,m-1);
  
  return scale;
  
}

static void spectrum(double *data, double *S, double *Sn, double *Smooth, double df, int N, double *fprior)
{
  double Dfmax, x;
  double Df1, Df2;
  int mw, k, i, j;
  int mm, kk;
  int end1, end2, end3;
  double *chunk;
  
  // log(2) is median/2 of chi-squared with 2 dof
  
  
  for(i=1; i< N/2; i++) S[i] = 2.0*(data[i]*data[i]+data[N-i]*data[N-i]);
  S[0] = S[1];
  
  Dfmax = 8.0; // is the  width of smoothing window in Hz
  
  // Smaller windows used initially where the spectrum is steep
  Df2 = Dfmax/2.0;
  Df1 = Dfmax/4.0;
  
  // defines the ends of the segments where smaller windows are used
  end1 = (int)(32.0/df);
  end2 = 2*end1;
  
  mw = (int)(Dfmax/df)+1;  // size of median window
  //printf("numer of bins in smoothing window %d\n", mw);
  k = (mw+1)/2;
  chunk = double_vector(mw);
  
  end3 = N/2-k;  // end of final chunk
  
  // Fill the array so the ends are not empty - just to be safe
  for(i=0;i< N/2;i++)
  {
    Sn[i] = S[i];
    Smooth[i] = S[i];
  }
  
  
  mw = (int)(Df1/df)+1;  // size of median window
  k = (mw+1)/2;
  
  for(i=4; i< k; i++)
  {
    mm = i/2;
    kk = (mm+1)/2;
    
    for(j=0;j< mm;j++)
    {
      chunk[j] = S[i-kk+j];
    }
    
    gsl_sort(chunk,1,mm);
    Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mm)/LN2; // chi-squared with two degrees of freedom
    Smooth[i] = Sn[i];
    
  }
  
  
  i = k;
  do
  {
    for(j=0;j< mw;j++)
    {
      chunk[j] = S[i-k+j];
    }
    
    gsl_sort(chunk,1,mw);
    Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mw)/LN2; // chi-squared with two degrees of freedom
    Smooth[i] = Sn[i];
    
    i++;
    
  }while(i < end1);
  
  
  
  mw = (int)(Df2/df)+1;  // size of median window
  k = (mw+1)/2;
  
  
  do
  {
    for(j=0;j< mw;j++)
    {
      chunk[j] = S[i-k+j];
    }
    
    gsl_sort(chunk,1,mw);
    Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mw)/LN2; // chi-squared with two degrees of freedom
    Smooth[i] = Sn[i];
    
    i++;
    
  }while(i < end2);
  
  mw = (int)(Dfmax/df)+1;  // size of median window
  k = (mw+1)/2;
  
  do
  {
    for(j=0;j< mw;j++)
    {
      chunk[j] = S[i-k+j];
    }
    
    gsl_sort(chunk,1,mw);
    Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mw)/LN2; // chi-squared with two degrees of freedom
    Smooth[i] = Sn[i];
    
    i++;
    
  }while(i < end3);
  
  
  for(i=end3; i< N/2-4; i++)
  {
    mm = (N/2-i)/2;
    kk = (mm+1)/2;
    
    for(j=0;j< mm;j++)
    {
      chunk[j] = S[i-kk+j];
    }
    
    gsl_sort(chunk,1,mm);
    Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mm)/LN2; // chi-squared with two degrees of freedom
    Smooth[i] = Sn[i];
    
  }

  for(i=N/2-4; i< N/2; i++) Smooth[i] = Sn[i] = Sn[N/2-4-1];

  
  free_double_vector(chunk);
  
  
  // zap the lines.
  for(i=1;i< N/2;i++)
  {
    x = S[i]/Sn[i];
    fprior[i] = 1.0;
    if(x > linemul)
    {
      Sn[i] = S[i];
      fprior[i] = 100.0;
    }
  }
  
}


static void whiten(double *data, double *Sn, int N)
{
  double x;
  int i;
  
  data[0] = 0.0;
  data[N/2] = 0.0;
  
  for(i=1; i< N/2; i++)
  {
    x = 1.0/sqrt(Sn[i]);
    data[i] *= x;
    data[N-i] *= x;
  }
  
}

void spectrum_lowlat(double *data, double *S, double *Sn, double *Smooth, double df, int N, double *fprior)
{
    double Df, Dfmax, x, y;
    double Df1, Df2;
    int mw, k, i, j;
    int mm, kk;
    double med;
    double *chunk;
    
    // log(2) is median/2 of chi-squared with 2 dof
    
    
    for(i=1; i< N/2; i++) S[i] = 2.0*(data[i]*data[i]+data[N-i]*data[N-i]);
    S[0] = S[1];
    
    Dfmax = 4.0; // is the  width of smoothing window in Hz
    

    
    mw = (int)(Dfmax/df)+1;  // size of median window
    //printf("numer of bins in smoothing window %d\n", mw);
    k = (mw+1)/2;
    chunk = double_vector(mw);
    
    // Fill the ends of the array so the ends are not empty
        Sn[0] = S[0];
        Smooth[0] = S[0];
    
    for(i=1; i< N/2; i++)
       {
           
        if(k > i)
        {
            kk = i;
            mm = 2*kk+1;
            
            for(j=0;j< mm;j++)
            {
                chunk[j] = S[i-kk+j];
            }
            
            gsl_sort(chunk, 1, mm);
            Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mm)/LN2;
            
        }
        else if (i > N/2-mw+k)
        {
            kk = N/2-1-i;
            mm = 2*kk+1;
            
            for(j=0;j< mm;j++)
             {
                           chunk[j] = S[i-kk+j];
              }
                       
               gsl_sort(chunk, 1, mm);
              Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mm)/LN2;
            
        }
        else
        {
           
        for(j=0;j< mw;j++)
        {
            chunk[j] = S[i-k+j];
        }
        
        gsl_sort(chunk, 1, mw);
        Sn[i] = gsl_stats_median_from_sorted_data(chunk, 1, mw)/LN2;
            
        }
        
        Smooth[i] = Sn[i];
        
    }
    
    
    free_double_vector(chunk);
    
    
    // zap the lines.
    for(i=1;i< N/2;i++)
    {
        x = S[i]/Sn[i];
        fprior[i] = 1.0;
        if(x > linemul)
        {
            Sn[i] = S[i];
            fprior[i] = 100.0;
        }
    }
    
    
    
}

static void clean(double *D, double *Draw, double *sqf, double *freqs, double *Sn, double *specD, double *sspecD, double df, double Q, double Tobs, double scale, double alpha, int Nf, int N, int imin, int imax, double *SNR, double *fprior, int Nthreads)
{
  
  int i, j, k;
  int flag;
  int ii, jj;
  double x;
  double S;

  // allocate some arrays
  double **tfDR, **tfDI;
  double **tfD;
  double **live;
  double **live2;
  
  double *Dtemp;
  
  live= double_matrix(Nf-1,N-1);
  live2= double_matrix(Nf-1,N-1);
  tfDR = double_matrix(Nf-1,N-1);
  tfDI = double_matrix(Nf-1,N-1);
  tfD = double_matrix(Nf-1,N-1);
  
  
  Dtemp = (double*)malloc(sizeof(double)*(N));
  
  for (i = 0; i < N; i++) Dtemp[i] = Draw[i];
  
  // D holds the previously cleaned data
  // Draw holds the raw data
  
  // D is used to compute the spectrum. A copy of Draw is then whitened using this spectrum and glitches are then identified
  // The glitches are then re-colored, subtracted from Draw to become the new D
  
  
  // Tukey window
  // tukey(D, alpha, N);
  // tukey(Dtemp, alpha, N);
  
  
  // FFT
  gsl_fft_real_radix2_transform(D, 1, N);
  gsl_fft_real_radix2_transform(Dtemp, 1, N);
  
  // Form spectral model for whitening data (lines plus a smooth component)
  if (lowlat == 0) spectrum(D, Sn, specD, sspecD, df, N, fprior);
  else if (lowlat == 1) spectrum_lowlat(D, Sn, specD, sspecD, df, N, fprior);
  
  // whiten data
  whiten(Dtemp, specD, N);
  
  // Wavelet transform
  TransformC(Dtemp, freqs, tfD, tfDR, tfDI, Q, Tobs, N, Nf, Nthreads);
  
  k = 0;
  //  apply threshold
  for(j = 0; j < Nf; j++)
  {
    for (i = 0; i < N; i++)
    {
      live[j][i] = -1.0;
      if(tfD[j][i] > sthresh) live[j][i] = 1.0;
      if(tfD[j][i] > sthresh) k++;
      live2[j][i] = live[j][i];
    }
  }
  
  
  // dig deeper to extract clustered power
  for(j = 1; j < Nf-1; j++)
  {
    for (i = 1; i < N-1; i++)
    {
      
      flag = 0;
      for(jj = -1; jj <= 1; jj++)
      {
        for(ii = -1; ii <= 1; ii++)
        {
          if(live[j+jj][i+ii] > 0.0) flag = 1;
        }
      }
      if(flag == 1 && tfD[j][i] > warm) live2[j][i] = 1.0;
    }
  }
  
  
  // build the excess power model
  for (i = 0; i < N; i++)
  {
    Dtemp[i] = 0.0;
  }
  
  k = 0;
  for(j = 0; j < Nf; j++)
  {
    for (i = imin; i < imax; i++)
    {
      if(live2[j][i] > 0.0) Dtemp[i] += scale*sqf[j]*tfDR[j][i];
    }
  }
  
  
  /*out = fopen("wglitch.dat", "w");
   for(i=0; i< N; i++)
   {
   fprintf(out,"%d %e\n", i, Dtemp[i]);
   }
   fclose(out);*/
  
  
  // Compute the excess power (relative to the current spectral model
  S = 0.0;
  for (i = imin; i < imax; ++i) S += Dtemp[i]*Dtemp[i];
  S = sqrt(S);
  
  printf("   Excess SNR = %f\n", S);
  
  *SNR = S;
  
  
  //Unwhiten and subtract the excess power so we can compute a better spectral estimate
  // Back to frequency domain
  
  gsl_fft_real_radix2_transform(Dtemp, 1, N);
  
  
  // only use smooth spectrum in the un-whitening
  Dtemp[0] = 0.0;
  for(i=1; i< N/2; i++)
  {
    x = sqrt(sspecD[i]);
    Dtemp[i] *= x;
    Dtemp[N-i] *= x;
  }
  Dtemp[N/2] = 0.0;
  
  gsl_fft_halfcomplex_radix2_inverse(Dtemp, 1, N);
  
  
  x = sqrt((double)(2*N));
  
  for(i=0; i< N; i++)
  {
    D[i] = Draw[i]-Dtemp[i]/x;
  }
  
  /*out = fopen("glitch.dat", "w");
   for(i=0; i< N; i++)
   {
   fprintf(out,"%d %e\n", i, Dtemp[i]/x);
   }
   fclose(out);*/
  
  
  free(Dtemp);
  free_double_matrix(live, Nf-1);
  free_double_matrix(live2,Nf-1);
  free_double_matrix(tfDR, Nf-1);
  free_double_matrix(tfDI, Nf-1);
  free_double_matrix(tfD,  Nf-1);
  
  
  return;
   
}

void isums(double x, double *is)
{
    double x2, x3, x4, x5;
    
    x2 = x*x;
    x3 = x2*x;
    
    is[0] = x;
    is[1] = (x2+x)/2.0;
    is[2] = (2.0*x3+3.0*x2+x)/6.0;
 
}


void solve(int n, double **M, double *av, double *yv)
{
    // solves the system M a = y for a
    
    int i, j, s;
    
    gsl_matrix *m = gsl_matrix_alloc (n, n);
    
    for (i = 0 ; i < n ; i++)
    {
        for (j = 0 ; j < n ; j++)
        {
            gsl_matrix_set(m, i, j, M[i][j]);
        }
    }
    
     gsl_vector *y = gsl_vector_alloc (n);
     
      for(i=0; i< n; i++) gsl_vector_set(y, i, yv[i]);
     
       gsl_vector *x = gsl_vector_alloc (n);
     
       gsl_permutation * p = gsl_permutation_alloc (n);
     
       gsl_linalg_LU_decomp (m, p, &s);
     
       gsl_linalg_LU_solve (m, p, y, x);
    
       for(i=0; i< n; i++) av[i] = gsl_vector_get(x, i);
     
       gsl_permutation_free (p);
       gsl_vector_free(x);
       gsl_vector_free(y);
       gsl_matrix_free(m);
    
       return;
}

void splinespace(int Ns, int istart, int iend, double *SM, double *freqs, double Tobs, double *splineA, double *splinef, int *Nk)
{
    int i, j, k, ii, jj, kk, kkold, n, Nx;
    int inc;
    double finc;
    double *lSM, *Sfit;
    double *im, *ym, *a;
    double *scale;
    double y2, cnt, xtol;
    double **M;
    double dfmax;
    double chisqquad, chisqlin, chisqconst;
    double x, z;
    FILE *out;
    
    im = double_vector(3);
    a = double_vector(2);
    ym = double_vector(2);
    M = double_matrix(2,2);
    
    finc = 1.0;  // frequency increment in fit
    dfmax = 256.0;  // minimum smooth spacing
    
    lSM = double_vector(Ns);
    Sfit = double_vector(Ns);
    
    for(i=istart; i<= iend; i++) lSM[i] = log(SM[i]);
    
    Nx = 1;
    splinef[0] = freqs[istart];
    splineA[0] = lSM[istart];
    
    
    inc = (int)(Tobs*dfmin);
    // total number of chunks
    k = (iend-istart)/inc;
    
    scale = double_vector(k);
    
    for(i=0; i< k; i++)
    {
        
        y2 = 0.0;
        for(jj=0; jj< 2; jj++) ym[jj] = 0.0;
        cnt = 0.0;

        for(j=0; j< inc; j++)
        {
            cnt += 1.0;
            ii = istart+i*inc+j;
            //printf("%d %d %d\n", i, ii, iend);
            y2 += lSM[ii]*lSM[ii];
            ym[0] += lSM[ii];
            ym[1] += cnt*lSM[ii];
        }
        
        
        isums(cnt, im);
        
        for(ii=0; ii< 2; ii++)
         {
             for(jj=0; jj< 2; jj++) M[ii][jj] = im[ii+jj];
         }
        
        n = 2;
        solve(n, M, a, ym);
        
        chisqlin = y2;
        for(ii=0; ii< n; ii++) chisqlin -= 2.0*a[ii]*ym[ii];
        for(ii=0; ii< n; ii++)
          {
              for(jj=0; jj< n; jj++) chisqlin += a[ii]*a[jj]*im[ii+jj];
          }
          
        z = chisqlin/cnt;
        
        scale[i] = z;
        
      }
    
    
    //
    
    // decide on the fit tolerance
    gsl_sort(scale, 1, k);
    jj = (int)(0.95*(double)(k));
    xtol = scale[jj];
    
    
    inc = (int)(Tobs*finc);
    // total number of chunks
    k = (iend-istart)/inc;
    
    
    y2 = 0.0;
    for(jj=0; jj< 2; jj++) ym[jj] = 0.0;
    cnt = 0.0;
    
    kkold = istart;
    
    for(i=0; i< k; i++)
    {

        for(j=0; j< inc; j++)
        {
            cnt += 1.0;
            ii = istart+i*inc+j;
            //printf("%d %d %d\n", i, ii, iend);
            y2 += lSM[ii]*lSM[ii];
            ym[0] += lSM[ii];
            ym[1] += cnt*lSM[ii];
        }
        
        
        isums(cnt, im);
        
        for(ii=0; ii< 2; ii++)
         {
             for(jj=0; jj< 2; jj++) M[ii][jj] = im[ii+jj];
         }
        
        n = 2;
        solve(n, M, a, ym);
        
        chisqlin = y2;
        for(ii=0; ii< n; ii++) chisqlin -= 2.0*a[ii]*ym[ii];
        for(ii=0; ii< n; ii++)
          {
              for(jj=0; jj< n; jj++) chisqlin += a[ii]*a[jj]*im[ii+jj];
          }
        
        x = cnt/Tobs;
        z = chisqlin/cnt;
        
        if(x >= dfmin)
         {
             
          if(z > xtol || x >= dfmax)
           {
            
           // if(x >= (dfmin+finc)) i -= 1; // wind back one increment
               
            kk = istart+(i+1)*inc-1;
        
            splinef[Nx] = freqs[kk];
            splineA[Nx] = lSM[kk];
               
            //printf("%d %d %d %f %e %e\n", i, kkold, kk, cnt, a[0], a[1]);
               
            // store the linear fit for this segment
            cnt = 0.0;
            for(ii=kkold; ii< kk; ii++)
            {
              cnt += 1.0;
              Sfit[ii] = a[0] + a[1]*cnt;
            }
               
            kkold = kk;
        
            Nx++;
        
        // restart the spacing
            y2 = 0.0;
            for(jj=0; jj< 2; jj++) ym[jj] = 0.0;
            cnt = 0.0;
            
          }
        }
        
        
        
      }
    
    if(kk < iend)
    {
    splinef[Nx] = freqs[iend];
    splineA[Nx] = lSM[iend];
    Nx++;
    }
    

        for(ii=kk; ii<= iend; ii++)
        {
          cnt += 1.0;
          Sfit[ii] = a[0] + a[1]*cnt;
        }
    
     if(Nx < 7) // need to add in some additional spline points
     {
       do
       {
           // find the knots with the largest spacing
           z = 0.0;
           for(i=0; i< Nx-1; i++)
           {
               x = splinef[i+1]-splinef[i];
               if(x > z)
               {
                   j = i;
                   z = x;
               }
           }

           x = (splinef[j+1]+splinef[j])/2.0;  // midway between the two points with largest spacing
           i = (int)(x*Tobs); // where it sits in the frequency array

           // add in a new spline knot
           splinef[Nx] = freqs[i];
           splineA[Nx] = lSM[i];
           Nx++;

           //sort the knots by frequency and apply the same ordering to Amplitudes
           gsl_sort2(splinef, 1, splineA, 1, Nx);


       }while(Nx < 7);

   }
    
    *Nk = Nx;
    printf("There are %d spline knots\n", Nx);
    
    free_double_matrix(M,2);
    free_double_vector(lSM);
    free_double_vector(Sfit);
    free_double_vector(a);
    free_double_vector(im);
    free_double_vector(ym);
    free_double_vector(scale);
    
}

void getrange(int k, int Nknot, double *ffit, double Tobs, int *imin, int *imax)
{
    // The original Akima solines used a 5 point stencil. The GSL Akima splines use
    // a 7 point stencil (maybe to make the fit C^2 versus C^1?)
    
    int flag;
    
    flag = 0;
     
    
              if(k > 3)
              {
                  *imin = (int)(ffit[k-3]*Tobs);
              }
              else
              {
                  *imin = (int)(ffit[0]*Tobs);
                  flag = 1;
              }
              
              if(k < Nknot-4)
              {
                  *imax = (int)(ffit[k+3]*Tobs);
              }
              else
              {
                  *imax = (int)(ffit[Nknot-1]*Tobs);
                  flag = 2;
              }
    
    //if(flag == 1) printf("%d %f\n", (int)(ffit[0]*Tobs), ffit[0]);
    
    if(flag == 1) *imax = (int)(ffit[6]*Tobs);
    if(flag == 2) *imin = (int)(ffit[Nknot-7]*Tobs);
    
}

double line(double f, double linef, double lineh, double linew, double deltafmax, double lineQ)
{
    double x, y, z;
    double ff2, f2, df2, df4;
    
             f2 = linef*linef;
    
             y = 0.0;
             x = fabs(f - linef);
             if(x < linew)
             {
             z = 1.0;
             if(x > deltafmax) z = exp(-(x-deltafmax)/deltafmax);
             ff2 = f*f;
             df2 = lineQ*(1.0-ff2/f2);
             df4 = df2*df2;
             y = z*lineh/(ff2/f2+df4);
             }
    
    return y;
                 
}

void lineset(int Ns, int istart, int iend, double *SM, double *PS, double *freqs, double Tobs, double *lf, double *lh, double *lQ, double *lw, double *dfmx, int max_lines, int *Nlns, int SnModel)
{
    int Nlines, flag, i, j, k, ii;
    double x, max, xold, spread;
    int min_len = 1000;
    int Nlinessearch = (max_lines > min_len ) ? max_lines : min_len ;
    double *lineh, *linew, *linef, *lineQ;
    linew = double_vector(Nlinessearch);
    lineh = double_vector(Nlinessearch);
    linef = double_vector(Nlinessearch);
    lineQ = double_vector(Nlinessearch);
  
  j = -1;
  xold = 1.0;
  flag = 0;
  for (i = istart; i < iend; ++i)
  {
      x = PS[i]/SM[i];
      // start of a line
      if(x > linemul && flag == 0)
      {
          k = 1;
          flag = 1;
          max = x;
          ii = i;
      }
      // in a line
      if((x > linemul) && flag ==1)
      {
          k++;
          if(x > max)
          {
              max = x;
              ii = i;
          }
      }
      // have reached the end of a line
      if(flag == 1)
      {
          if(x < linemul)
          {
              flag = 0;
              j++;
              linef[j] = freqs[ii];
              if(SnModel == 0)
              {
                  lineh[j] = (max-1.0)*SM[ii];
              }
              else
              {
                  lineh[j] = max-1.0;
              }
              lineQ[j] = sqrt(max)*linef[j]*Tobs/(double)(k);
              
                spread = (1.0e-2*lineQ[j]);
                if(spread < 50.0) spread = 50.0;  // maximum half-width is f_resonance/50
                dfmx[j] = linef[j]/spread;
                linew[j] = 8.0*dfmx[j];
             
          }
      }
      
  }
   Nlines = j+1;

    // If the maximum no.of allowed lines is less than what it found:
    if(Nlines > max_lines)
    {
      printf("Nlines = %d > maxlines %d \n", Nlines, max_lines);
      //Sort according to their height and select the tallest lines of len maxlines
      size_t * tall_lines_index = malloc (max_lines * sizeof(size_t));
      //Sort
      gsl_sort_largest_index(tall_lines_index, max_lines, lineh, 1, Nlines);
      for(i=0; i<max_lines; i++)
      {
          //copy
          lw[i] = linew[tall_lines_index[i]];
          lf[i] = linef[tall_lines_index[i]];
          lh[i] = lineh[tall_lines_index[i]];
          lQ[i] = lineQ[tall_lines_index[i]];
      }
      Nlines = max_lines;
      free (tall_lines_index);
     }
     else
     {
          printf("Nlines = %d < maxlines %d\n", Nlines, max_lines);
          //copy
          for(i=0; i<Nlines; i++)
          {
              lw[i] = linew[i];
              lf[i] = linef[i];
              lh[i] = lineh[i];
              lQ[i] = lineQ[i];
          }
      }
  
  // printf("There are %d lines\n", Nlines);
    
    *Nlns = Nlines;
    
    free(linew);
    free(lineh);
    free(linef);
    free(lineQ);

}

void makespec(double *SN, double *SM, double *SL, double *freqs, int istart, int iend, int Nknot, int Nlines, double *splinef, double *splineA, double *linef, double *lineh, double *linew, double *deltafmax, double *lineQ, int SnModel)
{
    int i, j;
    double f, y;
    gsl_spline   *aspline;
    gsl_interp_accel *acc;
    
    // smooth spectrum
    // Allocate spline
    aspline = gsl_spline_alloc(gsl_interp_akima, Nknot);
    acc = gsl_interp_accel_alloc();
    gsl_spline_init(aspline,splinef,splineA,Nknot);
    SM[istart] = exp(splineA[0]);
    SM[iend] = exp(splineA[Nknot-1]);
    for (i = istart+1; i < iend; ++i)
    {
        SM[i] = exp(gsl_spline_eval(aspline,freqs[i],acc));
    }
    gsl_spline_free (aspline);
    gsl_interp_accel_free (acc);
    
    // initialize line spectrum
    for (i = istart; i <= iend; ++i)
    {
        f = freqs[i];
        y = 0.0;
        for (j = 0; j < Nlines; ++j) y += line(f, linef[j], lineh[j], linew[j], deltafmax[j], lineQ[j]);
        SL[i] = y;
    }
    
    for (i = istart; i <= iend; ++i)
    {
        if(SnModel == 0)
        {
            SN[i] = SM[i]+SL[i];
        }
        else
        {
            SN[i] = SM[i]*(1.0+SL[i]);
        }
    }
    
}

void BayesSpec(double *PS, double *SNA, double *SMA, double *lf, double *lh, double *lQ, double *lw, double *splinef, double *splineA, int *Nknt, int *Nln, int max_lines, double Tobs, double fmin, double fmax, int SnModel, int Nthread)
{
    int i, j, k, mc, sc;
    int scount, sacc, hold, uc;
    int N, NC, NCC, NCH, Ns, MM, Nk;
    double **LarrayX;
    double *logLx;
    int *who;
    double *heat;
    double **splinefx, **splineAx;
    int imin, imax;
    int *acS, *acL, *cS, *cL;
    int typ, sm, sma;
    double **linef, **lineh, **lineQ;
    double *dfmx;
    double **SN, **SM, **SL;
    int istart, iend;
    double *freqs;
    int Nlines;
    int *Nknot;
    double smsc, lnsc, f;
    double hmul;
    double spread, **deltafmax, **linew;
    double x, y, z;
   double lmax;
   int qmax;
    int MXline = 10000;
    // int MXspline = 1000;

     double alpha, beta;

    const gsl_rng_type * T;
    gsl_rng * r;

    gsl_rng_env_setup();

    T = gsl_rng_default;
    r = gsl_rng_alloc (T);

    clock_t start, end;
    double cpu_time_used;

    double itime, ftime, exec_time;

    FILE *out;

    Ns = (int)(Tobs*fmax);

    istart = (int)(fmin*Tobs);
    iend = (int)(fmax*Tobs);
    if(iend > Ns-1) iend = Ns-1;
    
    smsc = 1.0;
    lnsc = 1.0;

    freqs = (double*)malloc(sizeof(double)*(Ns));
    for (i = 0; i < Ns; ++i)   freqs[i] = (double)(i)/Tobs;

    dfmx = double_vector(MXline);
    
    // these are used by the spline.
    gsl_spline   *aspline;
    gsl_interp_accel *acc;

    NC = Nthread;
    if(NC > 1) NCC = 2; 
    else NCC=1;
    NCH = NC-NCC;
    if(NCH < 0) NCH = 0;

    // printf("Using %d cold and %d hot chains\n", NCC, NCH);

    SL = double_matrix(NC,Ns);
    SM = double_matrix(NC,Ns);
    SN = double_matrix(NC,Ns);

    Nknot = int_vector(NC);
    
    //##############################################
    //open MP modifications, should be unnecessary?
    omp_set_num_threads(Nthread);
    // each chain gets its own random variable
    const gsl_rng_type * P;
    P = gsl_rng_default;
    rvec = (gsl_rng **)malloc(sizeof(gsl_rng *) * (NC+1));
    for(i = 0 ; i<= NC; i++){
        rvec[i] = gsl_rng_alloc(P);
        gsl_rng_set(rvec[i] , i);
    }
    //##############################################

    LarrayX = double_matrix(NC,Ns);
    logLx = double_vector(NC);
    who = int_vector(NC);
    heat = double_vector(NC);
    acS = int_vector(NC);
    acL = int_vector(NC);
    cS = int_vector(NC);
    cL = int_vector(NC);

    hmul = 1.1;

    for (i=0; i< NC; i++) who[i] = i;
    for (i=0; i< NCC; i++) heat[i] = 1.0;
    for (i=NCC; i< NC; i++) heat[i] = hmul*heat[i-1];

    
    splinespace(Ns, istart, iend, SMA, freqs, Tobs, splineA, splinef, &Nknot[0]);
    
    
    splinefx = double_matrix(NC,Nknot[0]);
    splineAx = double_matrix(NC,Nknot[0]);

    for (j = 0; j < NC; ++j)
    {
        Nknot[j] = Nknot[0];
        for (i = 0; i < Nknot[j]; ++i)
        {
            splineAx[j][i] = splineA[i];
            splinefx[j][i] = splinef[i];
        }
    }

    lineset(Ns, istart, iend, SMA, PS, freqs, Tobs, lf, lh, lQ, lw, dfmx, max_lines, &Nlines, SnModel);
    linef = double_matrix(NC,Nlines);  // central frequency
    lineh = double_matrix(NC,Nlines);  // line height
    lineQ = double_matrix(NC,Nlines); // line Q
    linew = double_matrix(NC,Nlines); // line width
    deltafmax = double_matrix(NC,Nlines); // cut-off

    for (j = 0; j < NC; ++j)
    {
        for (i = 0; i < Nlines; ++i)
        {
            linef[j][i] = lf[i];
            lineh[j][i] = lh[i];
            lineQ[j][i] = lQ[i];
            linew[j][i] = lw[i];
            deltafmax[j][i] = dfmx[i];
        }
    }

    
    // initialize smooth spectrum
    makespec(SN[0], SM[0], SL[0], freqs, istart, iend, Nknot[0], Nlines, splinefx[0], splineAx[0], linef[0], lineh[0], linew[0], deltafmax[0], lineQ[0], SnModel);
    for (j = 1; j < NC; ++j)
    {
        for (i = 0; i < Ns; ++i)
        {
            SM[j][i] = SM[0][i];
            SN[j][i] = SN[0][i];
            SL[j][i] = SL[0][i];
        }
    }

    for (i = istart; i <= iend; ++i)
    {
        LarrayX[0][i] = -(log(SN[0][i]) + PS[i]/SN[0][i]);
    }

    logLx[0] = 0.0;
    for (i=istart; i<= iend; i++) logLx[0] += LarrayX[0][i];

    for (j = 1; j < NC; ++j)
    {
        logLx[j] = logLx[0];
        for (i = istart; i <= iend; ++i) LarrayX[j][i] = LarrayX[0][i];
    }


    sc = 0;
    for (i = istart; i <= iend; ++i) SNA[i] = 0.0;

    sma = 0;
    for (i = istart; i <= iend; ++i) SMA[i] = 0.0;

    
    // max likelihood
    lmax = logLx[0];
    qmax = 0;

    MM = 200000;   // iterations of MCMC
    if(fmax > 4000.0) MM = 400000;
    if(fmax > 8000.0) MM = 800000;

    scount = 1;
    sacc = 0;
    for (i=0; i< NC; i++)
    {
        acS[i] = 0;
        acL[i] = 0;
        cS[i] = 0;
        cL[i] = 0;
    }

    itime = omp_get_wtime();

    uc = 0;
    
    for (mc = 1; mc < MM; ++mc)
    {

        
        if(mc > MM/25 && mc < (MM/2-10000) && mc%(MM/25) == 0)
        {

            // dynamic scaling of jumps (ok during burn-in)

            k = who[0];
            x = (double)acS[k]/(double)(cS[k]);
            if(x > 0.7) smsc *= 2.0;
            if(x < 0.4) smsc /= 2.0;

            k = who[0];
            x = (double)acL[k]/(double)(cL[k]);
            if(x > 0.7) lnsc *= 2.0;
            if(x < 0.4) lnsc /= 2.0;

            scount = 1;
            sacc = 0;
            for (i=0; i< NC; i++)
            {
                acS[i] = 0;
                acL[i] = 0;
                cS[i] = 1;
                cL[i] = 1;
            }

            // remove spline points that are not needed
            trim(splinefx, splineAx, Nknot, freqs, SN, SM, SL, PS, LarrayX, logLx, istart, iend, Tobs, NC, SnModel);
        }


        alpha = gsl_rng_uniform(r);

        if((NC > 1) && (alpha < 0.4))  // decide if we are doing a MCMC update of all the chains or a PT swap
        {
            // chain swap

            for(k=0; k < NC; k++)
            {
                alpha = (double)(NC-1)*gsl_rng_uniform(r);
                j = (int)(alpha);
                if(j > NCC-1) scount++;
                beta = exp((logLx[who[j]]-logLx[who[j+1]])/heat[j+1] - (logLx[who[j]]-logLx[who[j+1]])/heat[j]);
                alpha = gsl_rng_uniform(r);
                if(beta > alpha)
                {
                    hold = who[j];
                    who[j] = who[j+1];
                    who[j+1] = hold;
                    if(j > NCC-1) sacc++;
                }
            }

        }
        else      // MCMC update
        {
#pragma omp parallel for num_threads(Nthread) default(none) shared(mc, who, NC, istart, iend, logLx, heat, smsc, lnsc, Tobs, Ns, Nknot, Nlines, freqs, PS, LarrayX, SM, SL, SN, splinefx, splineAx, linef, lineh, lineQ, linew, deltafmax, cS, cL, acS, acL, SnModel, rvec)
//          #pragma omp parallel for
            for(k=0; k < NC; k++)
            {
                int q = who[k];
                psdstart_update(mc, k, q, istart, iend, logLx, heat[k], smsc, lnsc, Tobs, Ns, Nknot[q], Nlines, freqs, PS, LarrayX[q], SM[q], SL[q], SN[q], splinefx[q], splineAx[q], linef[q], lineh[q], lineQ[q], linew[q], deltafmax[q], cS, cL, acS, acL, SnModel, rvec[k]);
            }

        }

        /*
        if(verbose == 1)
        {
            k = who[0];
            if(mc%1000 == 0) printf("%d %e %f %f %f\n", mc, logLx[k], (double)acS[k]/(double)(cS[k]), (double)acL[k]/(double)(cL[k]), (double)sacc/(double)(scount));
        }
         */


        if(mc >= MM/2 && mc%200 == 0)
        {
            for(k=0; k < NCC; k++)
            {
                sma++;
                for (i = istart; i <= iend; ++i) SMA[i] += SM[who[k]][i];
            }

            for(k=0; k < NCC; k++)
            {
                sc++;
                for (i = istart; i <= iend; ++i) SNA[i] += SN[who[k]][i];
            }

        }
    }
    
//        if(verbose == 1) fclose(out);

    /*
    ftime = omp_get_wtime();
    exec_time = ftime - itime;
    printf("\nMCMC took %f seconds\n", exec_time);
    */

    for (i = istart; i <= iend; ++i)
    {
        SNA[i] /= (double)(sc);
        SMA[i] /= (double)(sma);
    }

    *Nknt = Nknot[0];
    *Nln = Nlines;
    printf("There are %d spline knots\nThere are %d lines\n", Nknot[0], Nlines);
    
    for (i = 0; i < Nlines; ++i)
    {
            lf[i] = linef[0][i];
            lh[i] = lineh[0][i];
            lQ[i] = lineQ[0][i];
            lw[i] = linew[0][i];
          dfmx[i] =  deltafmax[0][i];
    }
    
    for (i = 0; i < Nknot[0]; ++i)
    {
         splineA[i] = splineAx[0][i];
         splinef[i] = splinefx[0][i];
    }
    
//     gsl_spline_init(aspline,splinef,splineA,Nknot);
//     SM[0][istart] = exp(splineA[0]);
//     SM[0][iend] = exp(splineA[Nknot-1]);
//     for (i = istart+1; i < iend; ++i)
//     {
//         SM[0][i] = exp(gsl_spline_eval(aspline,freqs[i],acc));
//     }
    
//     // initialize line spectrum
//     for (i = istart; i <= iend; ++i)
//     {
//         f = freqs[i];
//         y = 0.0;
//         for (j = 0; j < Nlines; ++j) y += line(f, lf[j], lh[j], lw[j], dfmx[j], lQ[j]);
//         SL[0][i] = y;
//     }

//     for (i = istart; i <= iend; ++i)
//      {
//         if(SnModel == 0)
//         {
//           SN[0][i] = SM[0][i] + SL[0][i];
//         }
//         else
//         {
//             SN[0][i] = SM[0][i]*(1.0 + SL[0][i]);
//         }
//      }

   
//        out = fopen("specstart.dat","w");
//        for (i = istart; i <= iend; ++i)
//        {
//            fprintf(out,"%e %e %e %e\n", (double)(i)/Tobs, SM[0][i], SL[0][i], SN[0][i]);
//         }
//        fclose(out);


    free(freqs);

    free_double_matrix(splinefx,NC);
    free_double_matrix(splineAx,NC);
    free_double_matrix(lineh,NC);
    free_double_matrix(linef,NC);
    free_double_matrix(lineQ,NC);
    free_double_matrix(linew,NC);

    free_double_matrix(deltafmax,NC);
    free_double_matrix(SM,NC);
    free_double_matrix(SL,NC);
    free_double_matrix(SN,NC);
    free_double_matrix(LarrayX,NC);
    free_double_vector(heat);
    free_double_vector(logLx);
    free_int_vector(who);

    free_double_vector(dfmx);
    
    free_int_vector(Nknot);
    free_int_vector(acS);
    free_int_vector(acL);
    free_int_vector(cS);
    free_int_vector(cL);

}

void trim(double **splinefx, double **splineAx, int *Nknot, double *freqs, double **SN, double **SM, double **SL, double *PS, double **LarrayX, double *logLx, int istart, int iend, double Tobs, int NC, int SnModel)
{
    int i, j, k, q, NKmax, NK;
    int ist, ied;
    double x, y;
    double *SNy, *SMy;
    double *splineA, *splinef;
    double *sA, *sf;
    gsl_spline   *aspline;
    gsl_interp_accel *acc;
    
    SMy = double_vector(iend+1);
    SNy = double_vector(iend+1);
    
    NKmax = 0;
    for (q = 0; q < NC; ++q)
    {
        if(Nknot[q] > NKmax) NKmax = Nknot[q];
    }
    
    splinef = double_vector(NKmax);
    splineA = double_vector(NKmax);
    sf = double_vector(NKmax);
    sA = double_vector(NKmax);
    
    for (q = 0; q < NC; ++q)
    {
        
        if(Nknot[q] > 7)
       {
         
        y = logLx[q];
        
        aspline = gsl_spline_alloc(gsl_interp_akima, Nknot[q]-1);
        acc = gsl_interp_accel_alloc();
        
        sf[0] = splinefx[q][0];
        sA[0] = splineAx[q][0];
        
        NK = 1;
        j = 1;
        do
        {
            k = 0;
            for (i = 0; i < Nknot[q]; ++i)
            {
                if(i != j)
                {
                    splineA[k] = splineAx[q][i];
                    splinef[k] = splinefx[q][i];
                    k++;
                }
            }
            
            gsl_spline_init(aspline,splinef,splineA,Nknot[q]-1);
            
            getrange(j, (Nknot[q]-1), splinef, Tobs, &ist, &ied);
            
            for (i = ist; i <= ied; ++i) SMy[i] = exp(gsl_spline_eval(aspline,freqs[i],acc));
            
            x = 0.0;
            for (i = ist; i <= ied; ++i)
            {
                if(SnModel == 0)
                {
                    SNy[i] = SMy[i] + SL[q][i];
                }
                else
                {
                    SNy[i] = SMy[i]*(1.0 + SL[q][i]);
                }
                x +=  -(log(SNy[i]) + PS[i]/SNy[i]) - LarrayX[q][i];
            }
            
            
            if(x < -2.0)
            {
                sf[NK] = splinefx[q][j];
                sA[NK] = splineAx[q][j];
                NK++;
                j++;
            }
            else
            {
                // force acceptance of the next two points to avoid removing knots close to each other
                j++;
                if(j < (Nknot[q]-1))
                {
                    sf[NK] = splinefx[q][j];
                    sA[NK] = splineAx[q][j];
                    NK++;
                    j++;
                    if(j < (Nknot[q]-1))
                    {
                        sf[NK] = splinefx[q][j];
                        sA[NK] = splineAx[q][j];
                        NK++;
                        j++;
                    }
                }
            }
            
            //printf("%d %d %e \n", j, NK, x);
            
            
        }while(j < (Nknot[q]-1));
        
        
        if(NK > 6)
        {
            
            for (i = 0; i < NK; ++i)
            {
                splinefx[q][i] = sf[i];
                splineAx[q][i] = sA[i];
            }
            
            // get the end point
            splinefx[q][NK] = splinefx[q][Nknot[q]-1];
            splineAx[q][NK] = splineAx[q][Nknot[q]-1];
            Nknot[q] = NK+1;
            
            gsl_spline_free (aspline);
            gsl_interp_accel_free (acc);
            
            // now update the PSD model and the likelihood
            
            aspline = gsl_spline_alloc(gsl_interp_akima, Nknot[q]);
            acc = gsl_interp_accel_alloc();
            
            gsl_spline_init(aspline,splinefx[q],splineAx[q],Nknot[q]);
            
            for (i = istart; i <= iend; ++i) SM[q][i] = exp(gsl_spline_eval(aspline,freqs[i],acc));
            
            for (i = istart; i <= iend; ++i)
            {
                if(SnModel == 0)
                {
                    SN[q][i] = SM[q][i] + SL[q][i];
                }
                else
                {
                    SN[q][i] = SM[q][i]*(1.0 + SL[q][i]);
                }
                LarrayX[q][i]  =  -(log(SN[q][i]) + PS[i]/SN[q][i]);
            }
            
            logLx[q] = 0.0;
            for (j=istart; j<= iend; j++) logLx[q] += LarrayX[q][j];
            
            gsl_spline_free (aspline);
            gsl_interp_accel_free (acc);
            
        }
        
       }
    }
        
        free_double_vector(splinef);
        free_double_vector(splineA);
        free_double_vector(sf);
        free_double_vector(sA);
        free_double_vector(SNy);
        free_double_vector(SMy);
        
}

void psdstart_update(int mc, int m, int q, int istart, int iend, double *logLx, double heat, double smsc, double lnsc, double Tobs, int Ns, int Nknot, int Nlines, double *freqs, double *PS, double *LarrayX, double *SM, double *SL, double *SN, double *splinefx, double *splineAx, double  *linef, double *lineh, double *lineQ, double *linew, double *deltafmax, int *cS, int *cL, int *acS, int *acL, int SnModel, gsl_rng *r)
{
    double alpha, beta, H, logLy;
    int i, j, k, flag, jj;
    int typ;
    int imin, imax;
    double *splineAy, *splinefy;
    double *DS, *SNY, *LarrayY;
    double ylinew, ylinef, ylineh, ylineQ, ydeltafmax;
    double sqht, df, spread, x, y;
    
    gsl_spline   *aspline;
    gsl_interp_accel *acc;
    
    sqht = sqrt(heat);
    df = 1.0/Tobs;
    
    SNY = double_vector(Ns);
    LarrayY = double_vector(Ns);
    DS = double_vector(Ns);
    
    for (i = istart; i <= iend; ++i)
    {
        SNY[i] = SN[i];
        LarrayY[i] = LarrayX[i];
        DS[i] = 0.0;
    }
    

    
  alpha = gsl_rng_uniform(r);

  if(alpha < 0.6) // update spline
  {
      
        splineAy = double_vector(Nknot);
        splinefy = double_vector(Nknot);
      
        // Allocate spline
        aspline = gsl_spline_alloc(gsl_interp_akima, Nknot);
        acc = gsl_interp_accel_alloc();
      
      flag = 0;
    
   typ = 0;
   cS[m]++;
   
    
     for (i = 0; i < Nknot; ++i)
     {
         splineAy[i] = splineAx[i];
         splinefy[i] = splinefx[i];
     }
        
      // pick a knot to update
      k = (int)((double)(Nknot)*gsl_rng_uniform(r));
        
        alpha = gsl_rng_uniform(r);
      
        if(k > 0 && k < Nknot-1 && alpha > 0.5)  // lateral shift
        {
            x = smsc*gsl_ran_gaussian(r,3.0);
            
            splinefy[k] = splinefx[k] + x/Tobs;
            
            // check spacing is ok (also stops them switching order)
            if(splinefy[k]-splinefx[k-1] < dfmin) flag = 1;
            if(splinefx[k+1]-splinefy[k] < dfmin) flag = 1;
            
            jj = 0;
            
        }
        else  // update amplitudes
        {
             alpha = gsl_rng_uniform(r);
            
             if(alpha > 0.5)
             {
              splineAy[k] =  splineAx[k] + smsc*sqht*gsl_ran_gaussian(r,0.05);
                 jj = 1;
             }
             else if (alpha > 0.2)
             {
             splineAy[k] =  splineAx[k] + smsc*sqht*gsl_ran_gaussian(r,0.2);
                 jj = 2;
             }
            else
             {
            splineAy[k] =  splineAx[k] + smsc*sqht*gsl_ran_gaussian(r,1.0);
                 jj = 3;
            }
        }
      
    if(flag == 0)
    {

          gsl_spline_init(aspline,splinefy,splineAy,Nknot);
          getrange(k, Nknot, splinefy, Tobs, &imin, &imax);

          if(imin < istart) imin = istart;
          if(imax > iend) imax = iend;
        
          for (i = imin; i <= imax; ++i) DS[i] = exp(gsl_spline_eval(aspline,freqs[i],acc));
       
        for (i = imin; i <= imax; ++i)
         {
            if(SnModel == 0)
            {
              SNY[i] = DS[i] + SL[i];
            }
            else
            {
                SNY[i] = DS[i]*(1.0 + SL[i]);
            }
           LarrayY[i] =  -(log(SNY[i]) + PS[i]/SNY[i]);
         }
        logLy = logLx[q];
        for (i=imin; i<= imax; i++) logLy += (LarrayY[i]-LarrayX[i]);
        
        //printf("%d %d %e\n", mc, jj, logLy-logLx[q]);
    }
    else // flag = 1, out of prior range
    {
        logLy = -1.0e40;  // move fails
    }
        
    }
    else // line update
    {
        
       typ = 1;
       cL[m]++;
        
        // pick a line to update
                   k = (int)((double)(Nlines)*gsl_rng_uniform(r));
                    
                    alpha = gsl_rng_uniform(r);
                    if(alpha > 0.5)
                    {
                     ylinef = linef[k] + lnsc*sqht*gsl_ran_gaussian(r,df);
                      if(SnModel == 0)
                      {
                       ylineh = lineh[k]*(1.0+lnsc*sqht*gsl_ran_gaussian(r,0.02));
                      }
                      else
                      {
                        ylineh = lineh[k]+lnsc*sqht*gsl_ran_gaussian(r,0.02);
                      }
                     ylineQ = lineQ[k] + lnsc*sqht*gsl_ran_gaussian(r,1.0);
                    }
                    else if (alpha > 0.3)
                    {
                     ylinef = linef[k] + lnsc*sqht*gsl_ran_gaussian(r,2.0*df);
                     if(SnModel == 0)
                     {
                      ylineh = lineh[k]*(1.0+lnsc*sqht*gsl_ran_gaussian(r,0.05));
                     }
                     else
                     {
                       ylineh = lineh[k]+lnsc*sqht*gsl_ran_gaussian(r,0.05);
                     }
                     ylineQ = lineQ[k] + lnsc*sqht*gsl_ran_gaussian(r,2.0);
                    }
                    else
                    {
                     ylinef = linef[k] + lnsc*sqht*gsl_ran_gaussian(r,0.1*df);
                     if(SnModel == 0)
                     {
                      ylineh = lineh[k]*(1.0+lnsc*sqht*gsl_ran_gaussian(r,0.002));
                     }
                     else
                     {
                       ylineh = lineh[k]+lnsc*sqht*gsl_ran_gaussian(r,0.002);
                     }
                     ylineQ = lineQ[k] + lnsc*sqht*gsl_ran_gaussian(r,0.1);
                    }
        
       if(ylineQ < 10.0) ylineQ = 10.0;
       if(ylinef < freqs[istart] || ylinef > freqs[iend]) ylinef = linef[k];
        spread = (1.0e-2*ylineQ);
        if(spread < 50.0) spread = 50.0;  // maximum half-width is f_resonance/20
        ydeltafmax = ylinef/spread;
        ylinew = 8.0*ydeltafmax;
        
       // need to cover old and new line
        imin = (int)((linef[k]-linew[k])*Tobs);
        imax = (int)((linef[k]+linew[k])*Tobs);
        i = (int)((ylinef-ylinew)*Tobs);
        if(i < imin) imin = i;
        i = (int)((ylinef+ylinew)*Tobs);
        if(i > imax) imax = i;
        if(imin < istart) imin = istart;
        if(imax > iend) imax = iend;
        
       // proposed line contribution
       // have to recompute and remove current line since lines can overlap
        for (i = imin; i <= imax; ++i)
        {
            DS[i] = line(freqs[i], ylinef, ylineh, ylinew, ydeltafmax, ylineQ);
            DS[i] -= line(freqs[i], linef[k], lineh[k], linew[k], deltafmax[k], lineQ[k]);
        }
        
       
        for (i = imin; i <= imax; ++i)
         {
            if(SnModel == 0)
            {
              SNY[i] = SM[i] + SL[i] + DS[i];
            }
            else
            {
                SNY[i] = SM[i]*(1.0 + SL[i] + DS[i]);
            }
           LarrayY[i] =  -(log(SNY[i]) + PS[i]/SNY[i]);
         }
        logLy = logLx[q];
        for (i=imin; i<= imax; i++) logLy += (LarrayY[i]-LarrayX[i]);
        
    }


    H = (logLy-logLx[q])/heat;
    
    alpha = log(gsl_rng_uniform(r));

    if(H > alpha)
    {
        logLx[q] = logLy;
        
        if(typ == 0)
        {
        
        acS[m]++;
        splineAx[k] = splineAy[k];
        splinefx[k] = splinefy[k];
            
        for (i = imin; i <= imax; ++i)
        {
             SM[i] = DS[i]; // replace
             SN[i] = SNY[i];
             LarrayX[i] = LarrayY[i];
         }
        }
        
        if(typ == 1)
        {
        acL[m]++;
          
           linef[k] = ylinef;
           lineh[k] = ylineh;
           lineQ[k] = ylineQ;
           linew[k] = ylinew;
           deltafmax[k] = ydeltafmax;
           
          for (i = imin; i <= imax; ++i)
           {
               SL[i] += DS[i];   // delta this segment
               SN[i] = SNY[i];
               LarrayX[i] = LarrayY[i];
           }
            
        }
        
    }
    
    if(typ==0)
    {
        free_double_vector(splineAy);
        free_double_vector(splinefy);

        gsl_spline_free (aspline);
        gsl_interp_accel_free (acc);
        
    }
    

    
    free_double_vector(DS);
    free_double_vector(SNY);
    free_double_vector(LarrayY);
    

}

void blstart(double *data, double *residual, int N, double dt, double fmin, int *Nsp, int *Nl, double *dspline, double *pspline, double *linef, double *lineh, double *lineQ, int max_lines, char *ifo, double *fprior, int SplineFlag, int SnModel, int Nthread)
{
  int i, j, k, Nf,  ii;
  int Nlines, Nspline;
  int flag;
  int imin, imax;
  int istart, iend;
  double SNR, max;
  double Tobs, f, df, x, y, dx;
  double fmax, Q, fny, scale;
  double *freqs;
  double *Draw;
  double *D;
  double *Sn, *PS, *SM, *SN;
  double *specD, *sspecD;
  double *sqf;
  int subscale, octaves;
  double SNRold;
  double alpha;
  double t_rise, s1, s2, fac;
  double *linew;
  double mean, sd;
  
  time_t rawtime;
  struct tm * timeinfo;
  
  char filename[1024];
  
  FILE *outFile;
  
  Q = Qs;  // Q of transform
  Tobs = (double)(N)*dt;  // duration
  
  // Tukey window parameter. Flat for (1-alpha) of data
  //TODO: NEED TO MAKE THIS A PARAMETER WE CAN CONTROL AT THE COMMAND LINE
  t_rise = 0.4; // Standard LAL setting
  alpha = (2.0*t_rise/Tobs);
  
  df = 1.0/Tobs;  // frequency resolution
  fny = 1.0/(2.0*dt);  // Nyquist
  
  // Set the range of the spectrogram. fmin, fmax must be a power of 2
  fmax = fny;
  
  
  D = (double*)malloc(sizeof(double)* (N));
  Draw = (double*)malloc(sizeof(double)* (N));
  
  linew = (double*)malloc(sizeof(double)*(max_lines));
  
  // Copy data over
  for(i=0; i< N; i++)
  {
    Draw[i] = data[i];
    D[i] = data[i];
  }
  
  
  // Normalization factor
  tukey_scale(&s1, &s2, alpha, N);
  
  
  // Time series data is corrupted by the Tukey window at either end
  // imin and imax define the "safe" region to use
  imin = (int)(2.0*t_rise/dt);
  imax = N-imin;
  
  
  // Prepare to make spectogram
  
  // logarithmic frequency spacing
  subscale = 40;  // number of semi-tones per octave
  octaves = (int)(rint(log(fmax/fmin)/log(2.0))); // number of octaves
  Nf = subscale*octaves+1;
  freqs = (double*)malloc(sizeof(double)* (Nf));   // frequencies used in the analysis
  sqf = (double*)malloc(sizeof(double)* (Nf));
  dx = log(2.0)/(double)(subscale);
  x = log(fmin);
  for(i=0; i< Nf; i++)
  {
    freqs[i] = exp(x);
    sqf[i] = sqrt(freqs[i]);
    x += dx;
  }
  
  //printf("frequency layers = %d\n", Nf);
  
  scale = Getscale(freqs, Q, Tobs, fmax, N, Nf, Nthread);
  
  
  sspecD = (double*)malloc(sizeof(double)*(N/2));
  specD = (double*)malloc(sizeof(double)*(N/2));
  Sn = (double*)malloc(sizeof(double)*(N/2));
  PS = (double*)malloc(sizeof(double)*(N/2));
  SM = (double*)malloc(sizeof(double)*(N/2));
  SN = (double*)malloc(sizeof(double)*(N/2));
  
  SNRold = 0.0;
  clean(D, Draw, sqf, freqs, Sn, specD, sspecD, df, Q, Tobs, scale, alpha, Nf, N, imin, imax, &SNR, fprior, Nthread);
  
  // if big glitches are present we need to rinse and repeat
  i = 0;
  while(i < 5 && (SNR-SNRold) > 10.0)
  {
    SNRold = SNR;
    clean(D, Draw, sqf, freqs, Sn, specD, sspecD, df, Q, Tobs, scale, alpha, Nf, N, imin, imax, &SNR, fprior, Nthread);
    i++;
  }
    
  // re-compute the power spectrum using the cleaned data
  // tukey(D, alpha, N);
  
  gsl_fft_real_radix2_transform(D, 1, N);

  // Form spectral model for whitening data (lines plus a smooth component)
  if(lowlat == 0) spectrum(D, Sn, specD, sspecD, df, N, fprior);
  else if(lowlat == 1) spectrum_lowlat(D, Sn, specD, sspecD, df, N, fprior);
  
  fac = Tobs*Tobs/((double)(N)*(double)(N))/2.;
    
  for (i = 0; i < N/2; ++i) SM[i] = sspecD[i]*fac;
  for (i = 0; i < N/2; ++i) SN[i] = specD[i]*fac;
  for (i = 0; i < N/2; ++i) PS[i] = Sn[i]*fac;

   if(lowlat == 0) blstartsub(N, &Nspline, &Nlines, dspline, pspline, linef, lineh, lineQ, linew, max_lines, ifo, Sn, specD, sspecD, df, Tobs, fmin, fmax, fac, SplineFlag, SnModel);
  if(lowlat == 1) BayesSpec(PS, SN, SM, linef, lineh, lineQ, linew, pspline, dspline, &Nspline, &Nlines, max_lines, Tobs, fmin, fmax, SnModel, Nthread);
  
  //copy output of clean (D) into residual for output
  residual[0] = 0.0;
  residual[1] = 0.0;
  for(i=1; i< N/2; i++)
  {
    residual[2*i]   = D[i]   * sqrt(fac);
    residual[2*i+1] = D[N-i] * sqrt(fac);
  }
 
  imin = (int)(fmin*Tobs);
  imax = (int)(fmax*Tobs)-1;
  
  if(lowlat == 1)
  {
        
        sprintf(filename, "%s_start_PSD.dat", ifo);
        outFile = fopen(filename,"w");
        for (i = 0; i < N/2; ++i)
        {
        fprintf(outFile,"%.15e %.15e\n", (double)(i)/Tobs, SN[i]);
        }
        fclose(outFile);
        time ( &rawtime );
        timeinfo = localtime ( &rawtime );
        printf ("Avg PSD: Current local time and date: %s\n", asctime (timeinfo) );
     
  }
  
  *Nl = Nlines;
  *Nsp = Nspline;
  
  /* The information needed for the Bayesline algorithm is:
   Nlines  // number of lines
   linef   // line central frequency
   lineh   // line height;
   lineQ   // line Q;
   Nspline  // number of spline points
   pspline  // reference grid of spline control points
   dspline  // values at reference points
   */
  double deltafmax, spread, z;
  for (i = 1; i < N/2; ++i) Sn[i] = 0.0;
  for (i = imin; i < N/2; ++i)
  {
    f = (double)(i)/Tobs;
    for (j = 0; j < Nlines; ++j)
    {
      spread = (1.0e-2*lineQ[j]);
      if(spread < 50.0) spread = 50.0;  // maximum half-width is f_resonance/50
      deltafmax = linef[j]/spread;
      x = fabs(f - linef[j]);
      z =1.0;
      if(x < linew[j])
      {
        if(x > deltafmax) z = exp(-(x-deltafmax)/deltafmax);
        Sn[i] += z*lineh[j]*pow(linef[j],4.0)/(pow(f*linef[j],2.0)+lineQ[j]*lineQ[j]*pow((pow(linef[j],2.0)-pow(f,2.0)),2.0));
      }
    }
  }
  
  int Nint;
  double *xint, *yint;
  
  Nint = (int)((fmax-fmin)*Tobs);
  xint = (double*)malloc(sizeof(double)*(Nint));
  yint = (double*)malloc(sizeof(double)*(Nint));
  for (i = 0; i < Nint; ++i)
  {
    xint[i] = fmin+(double)(i)/Tobs;
  }
  
  if(SplineFlag == 0) CubicSplineGSL(0,Nint,Nspline, pspline, dspline, Nint, xint, yint);
  if(SplineFlag == 1) AkimaSplineGSL(0,Nint,Nspline, pspline, dspline, Nint, xint, yint);
    
  k = (int)(fmin*Tobs);
  sprintf(filename, "%s_fairdrawstart_psd.dat", ifo);
  outFile = fopen(filename,"w");
  for (i = 0; i < Nint+k; ++i)
  {
    if (i<k){
      fprintf(outFile,"%.15e %.15e\n", (double)(i)/Tobs, 1.);
    }
    else{
      if(SnModel == 0) fprintf(outFile,"%.15e %.15e\n", (double)(i)/Tobs, (Sn[i]+exp(yint[i-k]))/(Tobs/2.) );
      if(SnModel == 1) fprintf(outFile,"%.15e %.15e\n", (double)(i)/Tobs, (exp(yint[i-k])*(1.0+Sn[i]))/(Tobs/2.));
    }  
  }
  fclose(outFile);
  
  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  printf("Fairdraw PSD: Current local time and date: %s\n", asctime (timeinfo) );
    
  free(xint);
  free(yint);
    
  free(D);
  free(Draw);
  free(linew);
  free(sspecD);
  free(specD);
  free(Sn);
  free(PS);
  free(SN);
  free(SM);
  free(freqs);
  free(sqf);
  // free_double_vector(lw);
  // free_double_vector(lh);
  // free_double_vector(lf);
  // free_double_vector(lQ);
  // free_double_vector(splinef);
  // free_double_vector(splineA);
  
}

//TODO The Lorentzian model is defined in 3 different places!  That is not good!!
static void Lorentzian(double *Slines, double Tobs, lorentzianParams *lines, int ii, int N)
{
  int i;
  double f2,f4;
  double deltf;
  double freq,fsq, x, z, deltafmax, spread;
  double amplitude;
  int istart, istop, imid, idelt;
  
  double A = lines->A[ii];
  double Q = lines->Q[ii];
  double f = lines->f[ii];
  
  
  // here we figure out how many frequency bins are needed for the line profile
  imid = (int)((f)*Tobs);
  spread = (1.0e-2*Q);
  
  if(spread < 50.0) spread = 50.0;  // maximum half-width is f_resonance/50
  deltafmax = f/spread;
  deltf = 8.0*deltafmax;
  idelt = (int)(deltf*Tobs)+1;
  
  
  istart = imid-idelt;
  istop = imid+idelt;
  if(istart < 0)  istart = 0;
  if(istop > N/2) istop = N/2;
  
  
  
  // add or remove the old line
  f2=f*f;
  f4=f2*f2;
  amplitude = A*f4;//(f2*Q*Q);
  for(i=istart; i<istop; i++)
  {
    freq = (double)i/Tobs;
    fsq = freq*freq;
    x = fabs(f-freq);
    z = 1.0;
    if(x > deltafmax) z = exp(-(x-deltafmax)/deltafmax);
    
    //Slines[i] += z*amplitude/(fsq*(fsq-f2)*(fsq-f2));
    Slines[i] += z*amplitude/(f2*fsq+Q*Q*(fsq-f2)*(fsq-f2));
  }
  
}


static void gaussian_kernel_smoothing(double *data, int N, double sigma)
{
  int i,j;
  
  /* calculate smoothing kernel */
  int NK = (int)sigma*6*2+1; //size of Kernel (6 sigma either side of 0)
  int imid = (NK-1)/2;       //width of one side of kernel
  double K[NK+1];            //kernel

  for(i=0; i<=NK; i++)
  {
    K[i] = exp( -0.5 * (double)(i-imid) * (double)(i-imid)/sigma/sigma )/sqrt(2.*M_PI*sigma*sigma);
  }
  
  
  
  /* setup array for smoothed data */
  double *data_smoothed = malloc(N*sizeof(double));
  for(i=0; i<N; i++) data_smoothed[i] = data[i];
  
  /* convolve kernel with data */
  for(i=imid; i < N-imid; i++)
  {
    data_smoothed[i]=0.0;
    for(j=-imid; j<=imid; j++)
    {
      data_smoothed[i] += K[j+imid]*data[i+j] ;
    }
  }
  
  /* copy smoothed data into original array */
  for(i=0; i<N; i++) data[i] = data_smoothed[i];
  
  free(data_smoothed);
}

void blstartsub(int N, int *Nsp, int *Nl, double *dspline, double *pspline, double *linef, double *lineh, double *lineQ, double *linew, int max_lines, char *ifo, double *Sn, double *specD, double *sspecD, double df, double Tobs, double fmin, double fmax, double fac, int SplineFlag, int SnModel)
{
    double max;
    double f, x, dx;
    double xold, xnext, Abar;
    int i, ii, j, k;
    int imin, imax;
    int flag;
    int Nlines, Nspline;
    int min_len = 1000;
    int Nlinessearch = (max_lines > min_len ) ? max_lines : min_len;
    double *lh, *lw, *lf, *lQ;
    
    char filename[1024];
  
    FILE *outFile;
    
    lw = double_vector(Nlinessearch);
    lh = double_vector(Nlinessearch);
    lf = double_vector(Nlinessearch);
    lQ = double_vector(Nlinessearch);
    
    // set up the spline model
      
      Nspline = (int)((fmax-fmin)/fsp)+1;
      // make sure that the last point is at or below fmax
      f = fmin+(double)(Nspline-1)*fsp;
      k = (int)(rint(f*Tobs));
      //if(k > N/2-1) Nspline = Nspline-1;
      
      for (i = 0; i < Nspline; ++i)
      {
        f = fmin+(double)(i)*fsp;
        k = (int)(rint(f*Tobs));
        
        //catch k going out of bounds
        if(k<N/2)
        {
          pspline[i] = (double)(k)/Tobs;
          dspline[i] = log(sspecD[k]*fac);
        }
        else
        {
          pspline[i] = (double)(N/2-1)/Tobs;
          dspline[i] = log(sspecD[N/2-1]*fac);
        }
      }
    
      j = 0;
      xold = 1.0;
      flag = 0;
      imin = (int)(fmin*Tobs);
      for (i = imin; i < N/2-1; ++i)
      {
        x = specD[i]/sspecD[i];
        xnext = specD[i+1]/sspecD[i+1];
        // start of a line
        if((x > xold && x > 5.0) && flag == 0)
        {
          k = 1;
          flag = 1;
          max = x;
          ii = i;
        }
        // in a line
        if(x > 5.0 && x > xold && flag ==1)
        {
          k++;
          if(x > max)
          {
            max = x;
            ii = i;
          }
        }
        // have reached the end of a line
        if(flag == 1)
        {
          if(x < 5.0 || (x < xold && xnext > x))
          {
            flag = 0;
            lw[j] = (double)(k)/Tobs;
            lf[j] = (double)(ii)/Tobs;
            if(SnModel == 1) lh[j] = (max-1.0);
            else if(SnModel == 0) lh[j] = (max-1.0)*sspecD[ii]*fac;
            Abar = 0.5*(specD[ii+1]/sspecD[ii+1]+specD[ii-1]/sspecD[ii-1]);
            lQ[j] = sqrt((max/Abar-1.0))*lf[j]*Tobs;
            j++;
          }
        }
          
        xold = x;
      }
    
    Nlines = j;
    
    // If the maximum no.of allowed lines is less than what it found:
    if(Nlines > max_lines)
    {
      printf("Nlines = %d > maxlines %d  Nspline %d \n", Nlines, max_lines, Nspline);
      //Sort according to their height and select the tallest lines of len maxlines
      size_t * tall_lines_index = malloc (max_lines * sizeof(size_t));
      //Sort
      gsl_sort_largest_index(tall_lines_index, max_lines, lh, 1, Nlines);
      for(i=0; i<max_lines; i++)
      {
          //copy
          linew[i] = lw[tall_lines_index[i]];
          linef[i] = lf[tall_lines_index[i]];
          lineh[i] = lh[tall_lines_index[i]];
          lineQ[i] = lQ[tall_lines_index[i]];
      }
      Nlines = max_lines;
      free (tall_lines_index);
     }
     else
     {
          printf("Nlines = %d < maxlines %d Nspline %d\n", Nlines, max_lines,Nspline);
          //copy
          for(i=0; i<Nlines; i++)
          {
              linew[i] = lw[i];
              linef[i] = lf[i];
              lineh[i] = lh[i];
              lineQ[i] = lQ[i];
          }
      }
      printf("There are %d spline points\n", Nspline);
      printf("There are %d lines\n", Nlines);
    
      *Nsp = Nspline;
      *Nl = Nlines;
      
      free(lw);
      free(lh);
      free(lf);
      free(lQ);

}

void BayesLineBurnin(struct BayesLineParams *bayesline, double *timeData, double *freqData, char *ifo, double *fprior, int SplineFlag, int SnModel, int Nthread)
{
  //Initialize BayesLine data structures
  BayesLineInitialize(bayesline);


  /*********************************************************************************************
   The pspline array holds the locations of the control points (knots) used in the spline
   The dspline array holds the values of the log of smooth spectrum at the control points
   The linef array contains the central frequencies of the Lorentzians
   The lineh array contains the amplitudes of the Lorentzians
   The lineQ array contains the Q factors of the Lorentzians
   *********************************************************************************************/

  //shortcuts to members of BayesLine structure
  dataParams *data           = bayesline->data;
  lorentzianParams *lines    = bayesline->lines_x;
  splineParams *spline       = bayesline->spline_x;

  int i,j;
  int max_lines=bayesline->maxBLLines;
  //number of samples in the data
  double Tobs = data->Tobs;
  double fmax = data->fmax;
  double dt   = 1./data->cadence;

  int N = 2*(int)floor(fmax*Tobs);
  
  blstart(timeData, freqData, N, dt, data->fmin, &spline->n, &lines->n, spline->data, spline->points, lines->f, lines->A, lines->Q, max_lines, ifo, fprior, SplineFlag, SnModel, Nthread);
   
  //Work space for assembling PSD model
  double *f     = malloc(sizeof(double)*(N+1));
  double *logSn = malloc(sizeof(double)*(N+1));
  double *Sl    = malloc(sizeof(double)*(N+1));
  
  int imin = data->fmin*Tobs;

  
  for(i=0; i<N/2; i++)
  {
    f[i]     = (double)i/Tobs;
    logSn[i] = 1.0;
    Sl[i]    = 0.0;
    
    if(i<N/2-imin){
    bayesline->Sbase[i] = 1.0;
    bayesline->Sline[i] = 0.0;
    }
  }

  //interpolate spline points
  if(SplineFlag == 1) AkimaSplineGSL(0,N/2-imin,spline->n,spline->points,spline->data,N/2-imin,f+imin,logSn+imin); 
  else if(SplineFlag == 0) CubicSplineGSL(0,N/2-imin,spline->n,spline->points,spline->data,N/2-imin,f+imin,logSn+imin);
  
  
  //initialize work space for spline model
  bayesline->data->flow  = bayesline->data->fmin;

  for(i=0; i< bayesline->data->ncut; i++)
  {
    j = i + imin - bayesline->data->nmin;
    bayesline->power[j] = freqData[2*j]*freqData[2*j]+freqData[2*j+1]*freqData[2*j+1];
    bayesline->spow[i]  = bayesline->power[j];
    bayesline->sfreq[i] = bayesline->freq[j];
  }

  //form-up line model
  for(i=0; i<lines->n; i++)
  {
    lines->larray[i] = i;
    //printf("Line %i: %g %g\n",i,lines->f[i], lines->A[i]);
    Lorentzian(Sl, Tobs, lines, lines->larray[i], N);
    // Lorentzian(Sl, Tobs, lines, i, N);
  }
  
  //combine spline and line model
  for(i=0; i<N/2-imin; i++)
  {
    bayesline->Sbase[i] = exp(logSn[i+imin]);
    bayesline->Sline[i] = Sl[i+imin];
    
    if(SnModel == 1)bayesline->Snf[i] = bayesline->Sbase[i]*(1.0 + bayesline->Sline[i]); 
    else if(SnModel == 0)bayesline->Snf[i] = bayesline->Sbase[i] + bayesline->Sline[i];
    
  }
   
  //Use initial estimate of PSD to set priors
  double PRIORSCALE = 10.0;
  /*
   set the lower bound of the frequency prior to be a factor of PRIORSCALE
   lower than ~max sensitivty in the "bucket"
   */
  double f_bucket = 200.0; //Hz
  int i_bucket = (int)floor(f_bucket*Tobs);
    char filen[1024];
    FILE *outfile; double fval;
    sprintf(filen, "%s_lower_upper_priorpsd.dat", ifo);
    outfile = fopen(filen,"w");
  for(i=0; i<N/2-imin; i++)
  {
    bayesline->priors->sigma[i] = bayesline->Snf[i];
    bayesline->priors->mean[i]  = bayesline->Snf[i];
    if(bayesline->flatPriorFlag) bayesline->priors->lower[i] = bayesline->Sbase[i_bucket-imin] / (PRIORSCALE*10.);
    else                         bayesline->priors->lower[i] = bayesline->Sbase[i]/PRIORSCALE;
    //bayesline->priors->upper[i] = bayesline->Snf[i]   * PRIORSCALE;
  
    if(SnModel == 1) bayesline->priors->upper[i] = (bayesline->Sbase[i]*(1.0 + bayesline->Sline[i]*100.0))*PRIORSCALE;
    else if(SnModel == 0) bayesline->priors->upper[i] = (bayesline->Sbase[i] + bayesline->Sline[i]*100.0)*PRIORSCALE;
    
      
    fval = (double)(i+imin)*(1.0/Tobs); 
    fprintf(outfile,"%f %e %e\n", fval, log(bayesline->priors->lower[i]), log(bayesline->priors->lower[i]*100.0));
  }
  fclose(outfile);
  //Smooth PSD prior
  gaussian_kernel_smoothing(bayesline->priors->upper, N/2-imin, 1);

    
  free(f);
  free(logSn);
  free(Sl);
 
}
